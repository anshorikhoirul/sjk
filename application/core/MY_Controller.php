<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	function __construct(){
		date_default_timezone_set("Asia/Jakarta");
		parent::__construct();
		if((!$this->session->has_userdata('logged_in')) || (!$this->session->userdata('logged_in'))):
			redirect(base_url().'auth/login');
		endif;
	}
}