<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_Model');
	}
	
	public function index()
	{
    	$role = $this->session->userdata('role');
        $datamember = $this->User_Model->memberdata()->row();
		if ($role == 1) {
			$content = array(
				'page' => 'profile',
				'id' =>$datamember->id_guru,
				'nama_lengkap' => $datamember->nama_lengkap,
				'email' => $datamember->email,
				'notelp' => $datamember->telp,
			);
		} else if ($role == 2) {
			$content = array(
				'page' => 'profile',
				'id' =>$datamember->id_siswa,
				'nama_lengkap' => $datamember->nama_lengkap,
				'email' => $datamember->email,
				'tmpt_lhr' => $datamember->tmpt_lhr,
				'tgl_lhr' => $datamember->tgl_lhr,
				'jk' => $datamember->jk,
				'notelp' => $datamember->telp,
			);
		}
		$this->load->view('pages/template', $content);
	}

	public function update_profile(){
		$role = $this->session->userdata('role');
		$id = $this->input->post('id');
		if ($role == 1) {
			$data = array(
				'nama_lengkap' => $this->input->post('inputName'), 
				'email' => $this->input->post('inputEmail'),
				'telp' => $this->input->post('inputPhone')
			);
		} else {
			$data = array(
				'nama_lengkap' => $this->input->post('inputName'), 
				'email' => $this->input->post('inputEmail'),
				'tmpt_lhr' => $this->input->post('inputTmptLhr'),
				'tgl_lhr' => $this->input->post('inputTglLhr'),
				'jk' => $this->input->post('inputJK'),
				'telp' => $this->input->post('inputPhone')
			);
		}
		$this->User_Model->profile_update($id, $data);
		$notif = array(
			'type' => 'success', 
			'msg' => 'Data profile telah diupdate!', 
		);
		$this->session->set_flashdata($notif);
		redirect(base_url('profile'));
	}

	public function update_pass(){
		$email	= html_escape($this->input->post('inputEmail'));
		$oldpswd	= md5(html_escape($this->input->post('inputOldPass')));
		$newpswd	= md5(html_escape($this->input->post('inputNewPass')));
		$confpswd	= md5(html_escape($this->input->post('inputConfPass')));
		$datamember = array(
			'email' => $email, 
			'password' => $oldpswd, 
		);
		if ($this->User_Model->cek_member($datamember)) {
			if ($newpswd == $confpswd) {
				$data['password'] = $newpswd;
				if ($this->User_Model->setting_update($email, $data)) {
					$notif = array(
						'type' => 'success', 
						'msg' => 'Password berhasil diUpdate!', 
					);
					$this->session->set_flashdata($notif);
					redirect(base_url('profile'));	
				} else {
					$notif = array(
						'type' => 'warning', 
						'msg' => 'Database error, gagal melakukan update password!', 
					);
					$this->session->set_flashdata($notif);
					redirect(base_url('profile'));	
				}
			} else {
				$notif = array(
					'type' => 'warning', 
					'msg' => 'New Pass tidak sama dengan Confirm Pass, gagal melakukan update password!', 
				);
				$this->session->set_flashdata($notif);
				redirect(base_url('profile'));	
			}
		} else {
			$notif = array(
				'type' => 'warning', 
				'msg' => 'Old Pass tidak sesuai, gagal melakukan update password!', 
			);
			$this->session->set_flashdata($notif);
			redirect(base_url('profile'));	
		}
	}

	public function update_photo(){
		$nmfile = "file_".time();
		$config['upload_path'] 		= './assets/profilpict/'; 
		$config['allowed_types']	= 'gif|jpg|png|jpeg|bmp';
		$config['max_size']			= 3072;
		$config['max_width']		= 2000;
		$config['max_height']		= 2000;
		$config['file_name']		= $nmfile; 

		$this->load->library('upload', $config);
		$datamember = $this->User_Model->memberdata()->row();
		$oldpp = $datamember->profilpict;
		if (!empty($oldpp)) {
			unlink("./assets/profilpict/$oldpp");
		}
		if ($this->upload->do_upload('photoprofile')) {
			$pp = $this->upload->data();
            $data['profilpict'] = $pp['file_name'];
            $this->session->set_userdata('profilpict', $data['profilpict']);
            $role = $this->session->userdata('role');
            if ($role == 1) {
            	$id = $datamember->id_guru;
            } elseif ($role == 2) {
            	$id = $datamember->id_siswa;
            }
            if ($this->User_Model->profile_update($id, $data)) {
            	$notif = array(
					'type' => 'success', 
					'msg' => 'Foto profil berhasil diperbarui!', 
				);
            } else {
            	$notif = array(
					'type' => 'warning', 
					'msg' => 'Gagal update database foto profil!', 
				);
            }
		} else {
			$notif = array(
				'type' => 'warning', 
				'msg' => 'Gagal upload foto profil!', 
			);
		}

		$this->session->set_flashdata($notif);
		redirect(base_url('profile'));	
	}

}
