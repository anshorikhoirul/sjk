<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('User_Model');
        $datamember = $this->User_Model->memberdata()->row();
        $memberdata['nama_lengkap'] = $datamember->nama_lengkap;
        $memberdata['profilpict'] = $datamember->profilpict;
        $this->session->set_userdata($memberdata);
	}
	
	public function index()
	{
		$content['page'] = 'dashboard';
		$this->load->view('pages/template', $content);
	}
}
