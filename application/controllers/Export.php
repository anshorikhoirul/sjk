<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends MY_Controller {
	function __construct(){
		parent::__construct();
    $this->load->helper('url');
		$this->load->model('User_Model');
		$this->load->model('Kelas_Model');
		$this->load->model('Absenlog_Model');
	}

	public function excel($id_kelas = FALSE){
		$role = $this->session->userdata('role');
		if ($role == 1) {
			if ($id_kelas == FALSE) {
				$notif = array(
					'type' => 'info',
					'msg' => 'Silahkan klik tombol Save to Excel pada list Matapelajaran yang tersedia...!',
				);
				$this->session->set_flashdata($notif);
				redirect(base_url('absensi/rekap_absensi'));
			} else {
				$datamember = $this->User_Model->memberdata()->row();
	      $id_guru = $datamember->id_guru;
				if ($this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)) {
					// cek apakah kelas sudah pernah melakukan absensi atau Belum
					if ($this->Absenlog_Model->cek_listsiswa($id_kelas)) {
						$notif = array(
							'type' => 'info',
							'msg' => 'Gagal mendapatkan log absensi, Absensi kelas masih kosong!',
						);
						$this->session->set_flashdata($notif);
						redirect(base_url('absensi/rekap_absensi'));
					} else {
						//informasi data matapelajaran
						$datakelas = $this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru);
						//informasi data identitas guru
						$datamember = $this->User_Model->memberdata()->row();
						// mendapatkan data dari absen_log
						$datasiswa = $this->Absenlog_Model->get_listsiswa($id_kelas);
						$counter = 0;
						foreach ($datasiswa as $dtsiswa) {
							// memasukkan list nama siswa ke dalam array
							$listsiswa[] = $dtsiswa->nama_lengkap;
							$joinkelas_id = $dtsiswa->joinkelas_id;
							// mendapatkan jumlah sakit dan memasukkannya ke dalam array
							$status_S = $this->Absenlog_Model->get_status($joinkelas_id, 'S');
							$countS[] = $status_S;
							// mendapatkan jumlah Ijin dan memasukkannya ke dalam array
							$status_I = $this->Absenlog_Model->get_status($joinkelas_id, 'I');
							$countI[] = $status_I;
							// mendapatkan jumlah Alpha dan Belum Absen kemudian memasukkannya ke dalam array
							$status_A = $this->Absenlog_Model->get_status($joinkelas_id, 'A');
							$status_B = $this->Absenlog_Model->get_status($joinkelas_id, 'B');
							$status_AB = $status_A + $status_B;
							$countA[] = $status_AB;
							// mendapatkan jumlah Masuk dan memasukkannya ke dalam array
							$status_M = $this->Absenlog_Model->get_status($joinkelas_id, 'M');
							$countM[] = $status_M;

							// menghitung jumlah Absensi
							$JA = $status_S + $status_I + $status_AB + $status_M;
							$countJA[] = $JA;
							// menghitung persentase kehadiran
							$PK = $status_M/$JA*100;
							$countPK[] = $PK;

							$counter++;
						}


						// Fungsi untuk export to excel
						//load librarynya terlebih dahulu
						$this->load->library('Excel');
						$this->load->library('ExportExcel');

						//membuka template excel
						$inputFileName = 'assets/rekapabsen.xls';
						$objPHPExcel = ExportExcel::load($inputFileName);

						// proses input informasi mapel
						$objPHPExcel->setActiveSheetIndex(0);
						$objPHPExcel->getActiveSheet()->setCellValue('B3', $datakelas->kode_unik)
				                        ->setCellValue('B4', $datakelas->nama_mapel)
				                        ->setCellValue('B5', $datamember->nama_lengkap)
				                        ->setCellValue('D3', $datakelas->kelas)
																->setCellValue('D4', $datakelas->ta)
																->setCellValue('D5', $datakelas->semester);
						// menentukan lokasi baris untuk input
						$baseRow = 10;
						$r = 0;
						for ($i=0; $i < $counter; $i++) {
							// proses input data rekap absensi
							$row = $baseRow + $r;
							$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
							$objPHPExcel->getActiveSheet()
													->setCellValue('A'.$row, $r+1)
													->setCellValue('B'.$row, $listsiswa[$i])
													->setCellValue('C'.$row, $countS[$i])
													->setCellValue('D'.$row, $countI[$i])
													->setCellValue('E'.$row, $countA[$i])
													->setCellValue('F'.$row, $countM[$i])
													->setCellValue('G'.$row, $countJA[$i])
													->setCellValue('H'.$row, $countPK[$i].' %');
							$r++;
						}

						$objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);
						//set title pada sheet (me rename nama sheet)
				    $objPHPExcel->getActiveSheet()->setTitle('Rekap Absensi_'.$datakelas->nama_mapel);

						$file = 'rekapabsensi_'.$datakelas->nama_mapel.'_'.strtotime(date("Y-m-d H:i:s")).'.xls';

						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="'.$file.'"');
						header('Cache-Control: max-age=0');
						header('Cache-Control: max-age=1');
						header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
						header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
						header('Pragma: public'); // HTTP/1.0
						$objWriter = ExportExcel::createWriter($objPHPExcel, 'Excel5');
						$objWriter->save('php://output');
						redirect(base_url('absensi/rekap_absensi'));
					}
				} else {
					$notif = array(
						'type' => 'info',
						'msg' => 'ID Kelas Mapel tidak ditemukan di dalam daftar kelas Anda!',
					);
					$this->session->set_flashdata($notif);
					redirect(base_url('absensi/rekap_absensi'));
				}
			}
		}else{
			show_404($page = '', $log_error = TRUE);
		}

	}

}
