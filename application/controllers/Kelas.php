<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
        $this->load->model('User_Model');
				$this->load->model('Kelas_Model');
        $this->load->model('Absenlog_Model');
	}

	public function index() {
		$role = $this->session->userdata('role');
    	if ($role == 1) {
	    	$datamember = $this->User_Model->memberdata()->row();
	       	$id_guru = $datamember->id_guru;
			$content = array(
				'page' => 'kelas',
				'dkelas' => $this->Kelas_Model->tampil_data($id_guru)->result()
			);
    	} else if ($role == 2) {
    		$datamember = $this->User_Model->memberdata()->row();
	       	$id_siswa = $datamember->id_siswa;
    		$content = array(
				'page' => 'kelas',
				'dkelas' => $this->Kelas_Model->tampil_kelassiswa($id_siswa)->result()
			);
    	}

		$this->load->view('pages/template', $content);
	}

	public function tambah_kelas() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama-mapel', 'Nama Matapelajaran', 'required|max_length[20]');
		$this->form_validation->set_rules('kelas-mapel', 'Kelas Matapeplajaran', 'required|max_length[15]');
		$this->form_validation->set_rules('tahun-ajaran', 'Tahun Ajaran', 'required|max_length[10]');
		$this->form_validation->set_rules('semester', 'Semester', 'required');

		if($this->form_validation->run()){
        	$datamember = $this->User_Model->memberdata()->row();
        	$id = null;
        	$id_guru = $datamember->id_guru;
        	$token = substr(md5(mt_rand()),0,6);
        	$nama_mapel = html_escape($this->input->post('nama-mapel'));
        	$kelas = html_escape($this->input->post('kelas-mapel'));
        	$ta = html_escape($this->input->post('tahun-ajaran'));
        	$semester = html_escape($this->input->post('semester'));
        	$absen_status = 0;
        	$data = array(
        		'id' => $id,
        		'id_guru' => $id_guru,
        		'kode_unik' => $token,
        		'nama_mapel' => $nama_mapel,
        		'kelas' => $kelas,
        		'ta' => $ta,
        		'semester' => $semester,
        		'absen_status' => $absen_status
        	);

        	if ($this->Kelas_Model->tambah_kelas($data)) {
        		$notif = array(
					'type' => 'success',
					'msg' => 'Group Kelas Matapelajaran baru berhasil di tambahkan!',
				);
        	} else {
        		$notif = array(
					'type' => 'warning',
					'msg' => 'Gagal menambahkan Kelas Matapelajaran baru!',
				);
        	}
				$this->session->set_flashdata($notif);
				redirect(base_url('kelas'));
		} else {
			$content['page'] = 'kelas';
			$this->load->view('pages/template', $content);
		}
	}

	public function hapus_kelas($id) {
		$role = $this->session->userdata('role');
		$datamember = $this->User_Model->memberdata()->row();
	    $id_kelas = $id;
	    $id_guru = $datamember->id_guru;
		if ($this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)) {
			if ($this->Kelas_Model->cek_joinkelas($id_kelas)) {
				$notif = array(
					'type' => 'info',
					'msg' => 'Group kelas yang sudah terisi siswa tidak dapat dihapus, hanya kelas yang masih kosong yang dapat dihapus!',
				);
			} else {
				if ($this->Kelas_Model->hapus_kelas($id_kelas)) {
					$notif = array(
						'type' => 'success',
						'msg' => 'Group kelas kosong berhasil dihapus',
					);
				} else {
					$notif = array(
						'type' => 'warning',
						'msg' => 'Terjadi error ketika proses hapus, silahkan coba lagi!',
					);
				}
			}
		} else {
			$notif = array(
				'type' => 'warning',
				'msg' => 'Group kelas yang akan dihapus tidak tersedia!',
			);
		}

		$this->session->set_flashdata($notif);
		redirect(base_url('kelas'));
	}

	public function joinkelas() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kode-kelas', 'Kode Kelas', 'required|min_length[6]|max_length[6]');
		$datamember = $this->User_Model->memberdata()->row();
	    $id_siswa = $datamember->id_siswa;
		if($this->form_validation->run()){
			$kode_kelas = html_escape($this->input->post('kode-kelas'));
			if ($this->Kelas_Model->cek_kode($kode_kelas)) {
				$datakelas = $this->Kelas_Model->cek_kode($kode_kelas)->row();
	       		$id_kelas = $datakelas->id;
	       		$nama_mapel = $datakelas->nama_mapel;
	       		if ($this->Kelas_Model->cek_siswajoinkelas($id_siswa, $id_kelas)) {
	       			if ($this->Kelas_Model->joinkelas($id_siswa, $id_kelas)) {
	       				$notif = array(
							'type' => 'success',
							'msg' => 'Berhasil melakukan proses join ke kelas <kbd>'.$nama_mapel.'</kbd> dengan kode <kbd>'.$kode_kelas.'</kbd>!!',
						);
	       			} else {
		       			$notif = array(
							'type' => 'warning',
							'msg' => 'Gagal melakukan proses join ke kelas <kbd>'.$nama_mapel.'</kbd> dengan kode <kbd>'.$kode_kelas.'</kbd>, silahkan coba lagi!!',
						);
	       			}
	       		} else {
		       		$notif = array(
						'type' => 'warning',
						'msg' => 'Anda sudah pernah melakukan permintaan untuk bergabung di kelas <kbd>'.$nama_mapel.'</kbd> dengan kode <kbd>'.$kode_kelas.'</kbd> !!!',
					);
	       		}
			} else {
				$notif = array(
					'type' => 'warning',
					'msg' => 'Kode kelas <kbd>'.$kode_kelas.'</kbd> tidak tersedia, periksa kode invite!',
				);
			}
			$this->session->set_flashdata($notif);
			redirect(base_url('kelas'));
		} else {
    		$content = array(
				'page' => 'kelas',
				'dkelas' => $this->Kelas_Model->tampil_kelassiswa($id_siswa)->result()
			);
			$this->load->view('pages/template', $content);
		}
	}

	public function hapus_joinkelas($id){
		$datamember = $this->User_Model->memberdata()->row();
		$id_siswa = $datamember->id_siswa;
		if ($this->Kelas_Model->periksa_joinkelas($id, $id_siswa)) {
			$notif = array(
				'type' => 'info',
				'msg' => 'Tidak ada kelas untuk dihapus!',
			);
		} else {
			if ($this->Kelas_Model->hapus_joinkelas($id)) {
				$notif = array(
					'type' => 'success',
					'msg' => 'Berhasil menghapus kelas',
				);
			} else {
				$notif = array(
					'type' => 'warning',
					'msg' => 'Terjadi error ketika proses hapus, silahkan coba lagi!',
				);
			}
		}
		$this->session->set_flashdata($notif);
		redirect(base_url('kelas'));
	}

	public function kelas_guru($id_kelas, $id_siswa = null){
		$role = $this->session->userdata('role');
		if ($role == 1) {
			if (empty($id_siswa)) {
				$datamember = $this->User_Model->memberdata()->row();
			  $id_guru = $datamember->id_guru;
			  if($datakelas = $this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)){
			    	$nama_mapel = $datakelas->nama_mapel;
					    $content = array(
							'page' => 'kelasguru',
							'nama_mapel' => $nama_mapel,
							'id_kelas' => $id_kelas,
							'dsiswa' => $this->Kelas_Model->tampil_kelasguru($id_kelas)->result()
						);
						$this->load->view('pages/template', $content);
			  } else {
			    	$notif = array(
						'type' => 'warning',
						'msg' => 'Kelas yang ingin di buka tidak dapat ditemukan, silahkan periksa url anda!',
					);
					$this->session->set_flashdata($notif);
					redirect(base_url('kelas'));
			  }
			} else {
				$datamember = $this->User_Model->memberdata()->row();
			  $id_guru = $datamember->id_guru;
				if($datakelas = $this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)){
					$id_kelas = $datakelas->id;
					$nama_mapel = $datakelas->nama_mapel;
			    	if ($datasiswa = $this->Kelas_Model->cek_kelassiswa($id_kelas, $id_siswa)) {
							$id_joinkelas = $datasiswa->id;
							// lihat data log absen
							$dataabsen = $this->Absenlog_Model->lht_dataabsen($id_joinkelas);
							// lihat profil siswa
							$profilsiswa = $this->User_Model->lht_profilsiswa($id_siswa)->row();
							// tampilkan halaman
							$content = array(
								'page' => 'datasiswa',
								'profilsiswa' => $profilsiswa,
								'logabsen' => $dataabsen,
								'nama_mapel' => $nama_mapel,
							);
							$this->load->view('pages/template', $content);
			    	} else {
			    		$notif = array(
								'type' => 'info',
								'msg' => 'Tidak ditemukan detail siswa yang dicari!'
							);
						$this->session->set_flashdata($notif);
						redirect(base_url('kelas/kelas_guru/'.$id_kelas));
			    	}
			    } else {
			    	$notif = array(
							'type' => 'info',
							'msg' => 'Kelas yang dicari tidak ditemukan!',
						);
					$this->session->set_flashdata($notif);
					redirect(base_url('kelas'));
			    }
			}
		} else {
			show_404($page = '', $log_error = TRUE);
		}
	}


	public function kelas_siswa($id_kelas){
		$role = $this->session->userdata('role');
		if ($role == 2) {
			$datamember = $this->User_Model->memberdata()->row();
		    $id_siswa = $datamember->id_siswa;
				if ($jk = $this->Kelas_Model->cek_kelassiswa($id_kelas, $id_siswa)) {
					$id_joinkelas = $jk->id;
					$profilmapel = $this->User_Model->lht_profilgurumapel($id_kelas);
					$dataabsen = $this->Absenlog_Model->lht_dataabsen($id_joinkelas);
					$content = array(
						'page' => 'kelassiswa',
						'infomapel' => $profilmapel,
						'dabsen' => $dataabsen,
						'id_kelas' => $id_kelas
					);
					$this->load->view('pages/template', $content);
				}else{
					$notif = array(
						'type' => 'info',
						'msg' => 'Kelas tidak ditemukan!',
					);
					$this->session->set_flashdata($notif);
					redirect(base_url('kelas'));
				}
		} else {
			show_404($page = '', $log_error = TRUE);
		}
	}

	function approve($id_kelas, $id_siswa){
		$role = $this->session->userdata('role');
    	if ($role == 1) {
	    	$datamember = $this->User_Model->memberdata()->row();
	       	$id_guru = $datamember->id_guru;
			if ($this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)) {
				if($this->Kelas_Model->approve_join($id_kelas, $id_siswa)){
					$notif = array(
						'type' => 'success',
						'msg' => 'Berhasil merubah status, siswa diijinkan untuk bergabung dengan kelas matapelajaran!',
					);
				} else {
					$notif = array(
						'type' => 'warning',
						'msg' => 'Gagal mengupdate database!',
					);
				}
			} else {
				$notif = array(
					'type' => 'warning',
					'msg' => 'Terjadi kesalahan penulisan URL!',
				);
			}
			$this->session->set_flashdata($notif);
			redirect(base_url('kelas/kelas_guru/'.$id_kelas));

    	} else if ($role == 2) {
			show_404($page = '', $log_error = TRUE);
    	}

		// die(var_dump('ini fungsi approve'));
	}

	function reject($id_kelas, $id_siswa){
		$role = $this->session->userdata('role');
    	if ($role == 1) {
	    	$datamember = $this->User_Model->memberdata()->row();
	       	$id_guru = $datamember->id_guru;
			if ($this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)) {
				if($this->Kelas_Model->reject_join($id_kelas, $id_siswa)){
					$notif = array(
						'type' => 'warning',
						'msg' => 'Berhasil merubah status, siswa tidak diijinkan untuk bergabung dengan kelas matapelajaran!!',
					);
				} else {
					$notif = array(
						'type' => 'warning',
						'msg' => 'Gagal mengupdate database!',
					);
				}
			} else {
				$notif = array(
					'type' => 'warning',
					'msg' => 'Terjadi kesalahan penulisan URL!',
				);
			}
			$this->session->set_flashdata($notif);
			redirect(base_url('kelas/kelas_guru/'.$id_kelas));

    	} else if ($role == 2) {
			show_404($page = '', $log_error = TRUE);
    	}
	}

	function dismiss($id_kelas, $id_siswa){
		$role = $this->session->userdata('role');
    	if ($role == 1) {
	    	$datamember = $this->User_Model->memberdata()->row();
	       	$id_guru = $datamember->id_guru;
			if ($this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)) {
				if($this->Kelas_Model->dismiss_join($id_kelas, $id_siswa)){
					$notif = array(
						'type' => 'success',
						'msg' => 'Siswa berhasil dikeluarkan dari kelas!!',
					);
				} else {
					$notif = array(
						'type' => 'warning',
						'msg' => 'Gagal mengupdate database!',
					);
				}
			} else {
				$notif = array(
					'type' => 'warning',
					'msg' => 'Terjadi kesalahan penulisan URL!',
				);
			}
			$this->session->set_flashdata($notif);
			redirect(base_url('kelas/kelas_guru/'.$id_kelas));

    	} else if ($role == 2) {
			show_404($page = '', $log_error = TRUE);
    	}
	}

}
