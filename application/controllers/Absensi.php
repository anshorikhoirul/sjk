<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
        $this->load->model('User_Model');
        $this->load->model('Kelas_Model');
        $this->load->model('Absenlog_Model');
	}

	public function index()
	{
		redirect(base_url('absensi/absensi_harian'));
	}

	public function absensi_harian($kelas = FALSE)
	{
		$role = $this->session->userdata('role');
    	if ($role == 1) {
	    	$datamember = $this->User_Model->memberdata()->row();
	       	$id_guru = $datamember->id_guru;
    		if ($kelas === FALSE) {
					$content = array(
						'page' => 'absensiharian',
						'role' => $role,
						'dkelas' => $this->Kelas_Model->tampil_data($id_guru)->result(),
					);
    		} else {
    			if ($this->Kelas_Model->cek_kelasguru($kelas, $id_guru)) {
    				$datakelas = $this->Kelas_Model->cek_kelasguru($kelas, $id_guru);
	    			$dataabsensi = $this->Absenlog_Model->logabsen_view($kelas);
	    			$content = array(
						'page' => 'logabsen',
						'role' => $role,
						'dkelas' => $datakelas,
						'dabsen' => $dataabsensi->result()
					);
    			} else {
    				$notif = array(
						'type' => 'info',
						'msg' => 'ID Kelas tidak ditemukan di dalam daftar kelas Anda!',
					);
					$this->session->set_flashdata($notif);
					redirect(base_url('absensi/absensi_harian'));
    			}
    		}
    	} else if ($role == 2) {
				$datamember = $this->User_Model->memberdata()->row();
				$id_siswa = $datamember->id_siswa;
				$tgl_skrg = date("Y-m-d");
				$logabsen = $this->Absenlog_Model->cek_absenharian($tgl_skrg, $id_siswa);
				$content = array(
					'page' => 'logabsen',
					'role' => $role,
					'infomapel' => $logabsen
				);
    	}

		$this->load->view('pages/template', $content);
	}

	public function ganti_status($id_kelas = FALSE){
		$role = $this->session->userdata('role');
		if ($role == 1) {
			if ($id_kelas === FALSE) {
				$notif = array(
					'type' => 'warning',
					'msg' => 'Terjadi kesalahan penulisan URL!',
				);
				$this->session->set_flashdata($notif);
				redirect(base_url('absensi/absensi_harian'));
			} else{
				$datamember = $this->User_Model->memberdata()->row();
	       		$id_guru = $datamember->id_guru;
	       		if($datakelas = $this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)){
	       			$idkelas =$datakelas->id;
	       			$status = $datakelas->absen_status;
	       			if ($status == 1) {
	       				$new_status = 0;
	       			} else {
	       				$new_status = 1;
	       			}
	       			$this->Kelas_Model->ganti_status($idkelas, $new_status);
	       			$notif = array(
						'type' => 'success',
						'msg' => 'Ganti Status kelas berhasil dilakukan!',
					);
	       		} else {
	       			$notif = array(
						'type' => 'info',
						'msg' => 'Kelas yang akan di ganti statusnya tidak ditemukan dalam daftar kelas Anda!',
					);
	       		}
				$this->session->set_flashdata($notif);
				redirect(base_url('absensi/absensi_harian'));
			}
		} else {
			show_404($page = '', $log_error = TRUE);
		}
	}

	public function create_absen($idkelas){
		$tgl = date('Y-m-d');
		$jam = date ("H:i:s");
		$role = $this->session->userdata('role');
		if ($role == 1) {
			if ($this->Absenlog_Model->cek_tglabsen($idkelas, $tgl)) {
				if($q = $this->Absenlog_Model->generate_absen($idkelas, $tgl, $jam)){
					$notif = array(
						'type' => 'success',
						'msg' => 'Absensi untuk hari ini berhasil dibuat, silahkan cek pada bagian Log Absensi',
					);
				} else {
					$notif = array(
						'type' => 'warning',
						'msg' => 'Pembuatan absen tidak dapat dilakukan untuk saat ini, tidak ada siswa aktif dalam kelas!',
					);
				}
			} else{
				$notif = array(
					'type' => 'info',
					'msg' => 'Absensi untuk hari ini sudah ada, silahkan cek pada bagian Log Absensi!',
				);
			}
		}else{
			show_404($page = '', $log_error = TRUE);
		}
		$this->session->set_flashdata($notif);
		redirect(base_url('absensi/absensi_harian/'.$idkelas));
	}

	public function hapus_absen($idkelas, $tgl){
		$role = $this->session->userdata('role');
		$dateview = date("d M Y", $tgl);
		$datesql = date("Y-m-d", $tgl);
		if ($role == 1) {
			$datamember = $this->User_Model->memberdata()->row();
       		$id_guru = $datamember->id_guru;
       		if($datakelas = $this->Kelas_Model->cek_kelasguru($idkelas, $id_guru)){
       			$this->Absenlog_Model->hapus_absen($idkelas, $datesql);
       			$notif = array(
					'type' => 'success',
					'msg' => 'Berhasil menghapus absensi '.$dateview,
				);
       		}else{
       			$notif = array(
					'type' => 'info',
					'msg' => 'ID Kelas tidak ditemukan dalam daftar kelas anda!',
				);
       		}
		}else{
			show_404($page = '', $log_error = TRUE);
		}
		$this->session->set_flashdata($notif);
		redirect(base_url('absensi/absensi_harian/'.$idkelas));
	}

	public function log($id_kelas = FALSE, $tgl = FALSE){
		$role = $this->session->userdata('role');
		if ($role == 1) {
			$datamember = $this->User_Model->memberdata()->row();
	       	$id_guru = $datamember->id_guru;
	       	if($datakelas = $this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)){
	       		if ($tgl === FALSE) {
					$notif = array(
						'type' => 'warning',
						'msg' => 'Terjadi kesalahan penulisan url, periksa kembali url anda!',
					);
					$this->session->set_flashdata($notif);
					redirect(base_url('absensi/absensi_harian/'.$id_kelas));
				}else{
					$tgl_absensi = date('Y-m-d', $tgl);
					if ($this->Absenlog_Model->cek_tglabsen($id_kelas, $tgl_absensi)) {
						$notif = array(
							'type' => 'info',
							'msg' => 'Tidak ada absensi untuk tanggal '.date("d-m-Y", $tgl).' !',
						);
						$this->session->set_flashdata($notif);
						redirect(base_url('absensi/absensi_harian/'.$id_kelas));
					} else {
						$dateview = date("d M Y", $tgl);
						$dataabsensi = $this->Absenlog_Model->log_view($id_kelas, $tgl);
						$content = array(
							'page' => 'log',
							'tglabsensi' => $dateview,
							'kelasmapel' => $datakelas->nama_mapel,
							'dabsen' => $dataabsensi->result()
						);
					}
				}

       		} else {
       			$notif = array(
					'type' => 'info',
					'msg' => 'Kelas yang akan di ganti statusnya tidak ditemukan dalam daftar kelas Anda!',
				);
				$this->session->set_flashdata($notif);
				redirect(base_url('absensi/absensi_harian/'.$id_kelas));
       		}
		}else{
			show_404($page = '', $log_error = TRUE);
		}
		$this->load->view('pages/template', $content);
	}

	public function setmasuk($id_kelas = FALSE, $tgl = FALSE, $id = FALSE){
		$role = $this->session->userdata('role');
		if ($role == 1) {
			// cek hak akses guru sebelum melakukan absensi pada kelas yang memiliki id = $id_kelas
			$datamember = $this->User_Model->memberdata()->row();
	    $id_guru = $datamember->id_guru;
	    if ($this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)) {
					// periksa ketersediaan absensi pada tanggal = $tgl
					$tgl_absensi = date('Y-m-d', $tgl);
	       	if ($this->Absenlog_Model->cek_tglabsen($id_kelas, $tgl_absensi)) {
	       			$notif = array(
								'type' => 'info',
								'msg' => 'Tidak ada absensi untuk tanggal '.date("d-m-Y", $tgl).' !',
							);
							$this->session->set_flashdata($notif);
							redirect(base_url('absensi/absensi_harian/'.$id_kelas));
	       	} else {
	       			// periksa tanggal absensi apakah sudah lewat atau masih aktif
	       			$tgl_skrg = date('Y-m-d');
	       			if ($tgl_skrg > $tgl_absensi) {
	       				$notif = array(
									'type' => 'warning',
									'msg' => 'Waktu absensi sudah selesai, anda tidak dapat melakukan absensi untuk tanggal '.date("d-m-Y", $tgl).' !',
								);
								$this->session->set_flashdata($notif);
								redirect(base_url('absensi/absensi_harian/'.$id_kelas));
	       			}else{
								// cek id_log dan tanggal absensi
								if($this->Absenlog_Model->cek_idlog($id, $tgl_absensi)) {
									// cek nama siswa
									$profilsiswa = $this->Absenlog_Model->profilsiswa($id)->row();
									$namasiswa = $profilsiswa->nama_lengkap;
		       				// update status absensi
									$status_absen = 'M';
									if ($this->Absenlog_Model->update_status($id, $status_absen)) {
										$notif = array(
											'type' => 'success',
											'msg' => 'BERHASIL melakukan update absensi untuk <strong>'.$namasiswa.'</strong> dengan status absensi <b>MASUK</b>!',
										);
									}else{
										$notif = array(
											'type' => 'warning',
											'msg' => 'GAGAL melakukan update absensi untuk <strong>'.$namasiswa.'</strong> dengan status absensi <b>MASUK</b>!',
										);
									}
									$this->session->set_flashdata($notif);
									redirect(base_url('absensi/Log/'.$id_kelas.'/'.$tgl));
								} else {
									$notif = array(
										'type' => 'info',
										'msg' => 'Data siswa tidak ditemukan dalam daftar absensi tanggal '.date('d M Y', $tgl),
									);
									$this->session->set_flashdata($notif);
									redirect(base_url('absensi/Log/'.$id_kelas.'/'.$tgl));
								}
	       			}
	       	}
	    } else {
				$notif = array(
					'type' => 'info',
					'msg' => 'ID Kelas tidak ditemukan!',
				);
				$this->session->set_flashdata($notif);
				redirect(base_url('absensi/absensi_harian/'));
			}
		}else{
			// cek hak akses siswa sebelum melakukan absensi pada kelas yang memiliki id = $id_kelas
			$datamember = $this->User_Model->memberdata()->row();
	    $id_siswa = $datamember->id_siswa;
			if ($this->Kelas_Model->cek_kelassiswa($id_kelas, $id_siswa)) {
				// periksa ketersediaan absensi pada tanggal = $tgl
				$tgl_absensi = date('Y-m-d', $tgl);
				if ($this->Absenlog_Model->cek_tglabsen($id_kelas, $tgl_absensi)) {
						$notif = array(
							'type' => 'info',
							'msg' => 'Tidak ada absensi untuk tanggal '.date("d-m-Y", $tgl).' !',
						);
						$this->session->set_flashdata($notif);
						redirect(base_url('absensi/absensi_harian'));
				} else {
					// periksa tanggal absensi apakah sudah lewat atau masih aktif
					$tgl_skrg = date('Y-m-d');
					if ($tgl_skrg > $tgl_absensi) {
						$notif = array(
							'type' => 'warning',
							'msg' => 'Tanggal absensi sudah lewat, anda tidak dapat melakukan absensi untuk tanggal '.date("d-m-Y", $tgl).' !',
						);
						$this->session->set_flashdata($notif);
						redirect(base_url('absensi/absensi_harian/'));
					}else{
						//periksa status absensi kelas apakah diaktifkan atau di nonaktifkan oleh guru
						if ($this->Kelas_Model->cek_absenstatus($id_kelas)) {


							// cek id_log dan tanggal absensi
							if($this->Absenlog_Model->cek_idlog($id, $tgl_absensi)) {
								// update status absensi
								$status_absen = 'M';
								if ($this->Absenlog_Model->update_status($id, $status_absen)) {
									$notif = array(
										'type' => 'success',
										'msg' => 'BERHASIL melakukan update absensi!',
									);
								}else{
									$notif = array(
										'type' => 'warning',
										'msg' => 'GAGAL melakukan update absensi!',
									);
								}
								$this->session->set_flashdata($notif);
								redirect(base_url('absensi/absensi_harian/'));
							} else {
								$notif = array(
									'type' => 'info',
									'msg' => 'TIdak ditemukan data siswa dalam daftar absensi tanggal '.date('d M Y', $tgl),
								);
								$this->session->set_flashdata($notif);
								redirect(base_url('absensi/absensi_harian/'));
							}



						} else {
							$notif = array(
								'type' => 'warning',
								'msg' => 'Tahap absensi sudah di nonaktifkan, anda tidak dapat melakukan absensi!',
							);
							$this->session->set_flashdata($notif);
							redirect(base_url('absensi/absensi_harian/'));
						}
					}
				}
			} else {
				$notif = array(
					'type' => 'info',
					'msg' => 'ID Kelas tidak ditemukan!',
				);
				$this->session->set_flashdata($notif);
				redirect(base_url('absensi/absensi_harian/'));
			}
		}
	}

	public function setabsent($id_kelas, $tgl, $id, $status){
		$role = $this->session->userdata('role');
		if ($role == 1) {
			// cek hak akses guru sebelum melakukan absensi pada kelas yang memiliki id = $id_kelas
			$datamember = $this->User_Model->memberdata()->row();
	    $id_guru = $datamember->id_guru;
	    if ($this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)) {
					// periksa ketersediaan absensi pada tanggal = $tgl
					$tgl_absensi = date('Y-m-d', $tgl);
	       	if ($this->Absenlog_Model->cek_tglabsen($id_kelas, $tgl_absensi)) {
	       			$notif = array(
								'type' => 'info',
								'msg' => 'Tidak ada absensi untuk tanggal '.date("d-m-Y", $tgl).' !',
							);
							$this->session->set_flashdata($notif);
							redirect(base_url('absensi/absensi_harian/'.$id_kelas));
	       	} else {
	       			// periksa tanggal absensi apakah sudah lewat atau masih aktif
	       			$tgl_skrg = date('Y-m-d');
	       			if ($tgl_skrg > $tgl_absensi) {
	       				$notif = array(
									'type' => 'warning',
									'msg' => 'Waktu absensi sudah selesai, anda tidak dapat melakukan absensi untuk tanggal '.date("d-m-Y", $tgl).' !',
								);
								$this->session->set_flashdata($notif);
								redirect(base_url('absensi/absensi_harian/'.$id_kelas));
	       			}else{
								// cek id_log dan tanggal absensi
								if ($this->Absenlog_Model->cek_idlog($id, $tgl_absensi)) {
									// cek nama siswa
									$profilsiswa = $this->Absenlog_Model->profilsiswa($id)->row();
									$namasiswa = $profilsiswa->nama_lengkap;
									if ($status == 's' || $status == 'i' || $status == 'a') {
										if ($status == 's') {
											$status_absen = 'S';
											$ket = 'SAKIT';
											$type = 'warning';
										} elseif ($status == 'i') {
											$status_absen = 'I';
											$ket = 'IZIN';
											$type = 'info';
										} elseif ($status == 'a') {
											$status_absen = 'A';
											$ket = 'TIDAK MASUK TANPA KETERANGAN';
											$type = 'danger';
										}
										if ($this->Absenlog_Model->update_absent($id, $status_absen)) {
											$notif = array(
												'type' => $type,
												'msg' => 'BERHASIL melakukan update absensi untuk <strong>'.$namasiswa.'</strong> dengan status absensi <b>'.$ket.'</b>!',
											);
										} else {
											$notif = array(
												'type' => $type,
												'msg' => 'GAGAL melakukan update absensi untuk <strong>'.$namasiswa.'</strong> dengan status absensi <b>'.$ket.'</b>!',
											);
										}
										$this->session->set_flashdata($notif);
										redirect(base_url('absensi/Log/'.$id_kelas.'/'.$tgl));

									}else{
										$notif = array(
											'type' => 'warning',
											'msg' => 'Terjadi kesalahan penulisan url, periksa kembali url anda!',
										);
										$this->session->set_flashdata($notif);
										redirect(base_url('absensi/Log/'.$id_kelas.'/'.$tgl));
									}
								} else {
									$notif = array(
										'type' => 'warning',
										'msg' => 'Data siswa tidak ditemukan dalam daftar absensi tanggal '.date('d M Y', $tgl),
									);
									$this->session->set_flashdata($notif);
									redirect(base_url('absensi/Log/'.$id_kelas.'/'.$tgl));
								}
	       			}
	       	}

	    } else {
				$notif = array(
					'type' => 'info',
					'msg' => 'ID Kelas tidak ditemukan di dalam daftar kelas Anda!',
				);
				$this->session->set_flashdata($notif);
				redirect(base_url('absensi/absensi_harian/'));
			}
		}else{
			show_404($page = '', $log_error = TRUE);
		}
	}

	public function rekap_absensi($id_kelas = FALSE){
		$role = $this->session->userdata('role');
		if ($role == 1) {
			if ($id_kelas == FALSE) {
				$datamember = $this->User_Model->memberdata()->row();
				$id_guru = $datamember->id_guru;
				$content = array(
					'page' => 'rekap',
					'role' => $role,
					'dkelas' => $this->Kelas_Model->tampil_data($id_guru)->result(),
				);
				$this->load->view('pages/template', $content);
			} else {
				$datamember = $this->User_Model->memberdata()->row();
	      $id_guru = $datamember->id_guru;
				if ($this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru)) {
					// cek apakah kelas sudah pernah melakukan absensi atau Belum
					if ($this->Absenlog_Model->cek_listsiswa($id_kelas)) {
						$notif = array(
							'type' => 'info',
							'msg' => 'Gagal mendapatkan log absensi, Absensi kelas masih kosong!',
						);
						$this->session->set_flashdata($notif);
						redirect(base_url('absensi/rekap_absensi'));
					} else {
						$datakelas = $this->Kelas_Model->cek_kelasguru($id_kelas, $id_guru);
						$datasiswa = $this->Absenlog_Model->get_listsiswa($id_kelas);
						// var_dump($datasiswa);
						$counter = 0;
						foreach ($datasiswa as $dtsiswa) {
							// memasukkan list nama siswa ke dalam array
							$listsiswa[] = $dtsiswa->nama_lengkap;
							$joinkelas_id = $dtsiswa->joinkelas_id;
							// mendapatkan jumlah sakit dan memasukkannya ke dalam array
							$status_S = $this->Absenlog_Model->get_status($joinkelas_id, 'S');
							$countS[] = $status_S;
							// mendapatkan jumlah Ijin dan memasukkannya ke dalam array
							$status_I = $this->Absenlog_Model->get_status($joinkelas_id, 'I');
							$countI[] = $status_I;
							// mendapatkan jumlah Alpha dan Belum Absen kemudian memasukkannya ke dalam array
							$status_A = $this->Absenlog_Model->get_status($joinkelas_id, 'A');
							$status_B = $this->Absenlog_Model->get_status($joinkelas_id, 'B');
							$status_AB = $status_A + $status_B;
							$countA[] = $status_AB;
							// mendapatkan jumlah Masuk dan memasukkannya ke dalam array
							$status_M = $this->Absenlog_Model->get_status($joinkelas_id, 'M');
							$countM[] = $status_M;

							// menghitung jumlah Absensi
							$JA = $status_S + $status_I + $status_AB + $status_M;
							$countJA[] = $JA;
							// menghitung persentase kehadiran
							$PK = $status_M/$JA*100;
							$countPK[] = $PK;

							$counter++;
						}

						$content = array(
							'page' => 'rekapkelas',
							'role' => $role,
							'dkelas' => $datakelas,
							'listsiswa' => $listsiswa,
							'countS' => $countS,
							'countI' => $countI,
							'countA' => $countA,
							'countM' => $countM,
							'countJA' => $countJA,
							'countPK' => $countPK,
							'counter' => $counter
						);
						$this->load->view('pages/template', $content);
					}
				} else {
					$notif = array(
					'type' => 'info',
					'msg' => 'ID Kelas Mapel tidak ditemukan di dalam daftar kelas Anda!',
				);
				$this->session->set_flashdata($notif);
				redirect(base_url('absensi/rekap_absensi'));
				}
			}
		} else {
			show_404($page = '', $log_error = TRUE);
		}
	}

}
