<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if (($this->session->has_userdata('logged_in')) && ($this->session->userdata('logged_in'))):
			redirect(base_url('dashboard'));
		endif;
		$this->load->model('Auth_Model');
		$this->load->model('Email_Model');
	}
	
	public function login(){
		$this->load->view('login/login_view');
	}

	public function register($param){
		if ($param == 'guru' || $param == 'siswa') {
			$data['jenisuser'] = $param;
			$this->load->view('login/register_view', $data);
		} else {
			show_404($page = '', $log_error = TRUE);
		}
	}

	public function submit_register(){
		$this->load->library('form_validation');
		// $jenisuser = $this->input->post('jenisuser');
		if ($this->input->post('jenisuser') == 'siswa') {
			// ini submit register siswa
			$this->form_validation->set_rules('fullname', 'Fullname', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[member.email]');
			$this->form_validation->set_rules('placeofbirth', 'Place of Birth', 'required');
			$this->form_validation->set_rules('dateofbirth', 'Date of Birth', 'required');
			$this->form_validation->set_rules('gender', 'Gender', 'required');
			$this->form_validation->set_rules('phonenumber', 'Phone Number', 'required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
		} elseif ($this->input->post('jenisuser') == 'guru') {
			// ini submit register guru
			$this->form_validation->set_rules('fullname', 'Fullname', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[member.email]');
			$this->form_validation->set_rules('phonenumber', 'Phone Number', 'required|max_length[13]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
		} else {
			show_404($page = '', $log_error = TRUE);
		}

		if($this->form_validation->run()) {
			$token = substr(md5(mt_rand()),0,15);
			$email = html_escape($this->input->post('email'));
			$this->Auth_Model->register($token);
			if($this->Email_Model->config()) {
				$this->load->library('email');
				$emailconfig = $this->Email_Model->config();
				$link = base_url("auth/activation/$token");
				// $message = "Please click on this link to activate your account ".$link;
				$message = 'Dear User,<br /><br />Please click on the below activation link to verify your email address.<br /><br />' .$link . '<br /><br /><br />Thanks<br />Admin SJK System';
				$smtp_user = $emailconfig->smtp_user;
				$emailname = $emailconfig->email_name;
				$emailsubject = $emailconfig->email_subject;
				$config = array(
					'protocol' => $emailconfig->protocol, 
					'smtp_crypto'=> $emailconfig->smtp_crypto,
					'smtp_host' => $emailconfig->smtp_host, 
					'smtp_port' => $emailconfig->smtp_port, 
					'smtp_user' => $emailconfig->smtp_user, 
					'smtp_pass' => $emailconfig->smtp_pass, 
					'mailtype' => $emailconfig->mailtype, 
					'charset' => $emailconfig->charset, 
					'wordwrap' => TRUE, 
					'newline' => "\r\n", 
				);

				date_default_timezone_set('Asia/Jakarta');
				$this->email->initialize($config);
				$this->email->from($smtp_user, $emailname);
				$this->email->to($email);
				$this->email->subject($emailsubject);
				$this->email->message($message);
				$this->email->send();
				// echo $this->email->print_debugger();
				$this->load->view('login/success');
			} else {
				$this->session->set_flashdata('msg','Pendaftaran Sukses tetapi pengiriman Kode Verifikasi EMail GAGAL, Hubungi ADMIN untuk aktivasi MANUAL!!!');
				redirect(base_url('auth/login'));
			}
		}else{
			// notifikasi error
			$data['jenisuser'] = $this->input->post('jenisuser');
			$this->load->view('login/register_view', $data);
		}
	}

	public function accountrecovery(){
		$this->load->view('login/accountrecovery_view');
	}

	function run(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if($this->form_validation->run() == FALSE) {
			$this->load->view('login/login_view');
		} else {
			if($this->Auth_Model->login()) {
				$hasil = $this->Auth_Model->login();
				if ($hasil) {
					$member_data = array(
						'email' => $hasil->email, 
						'role' => $hasil->role, 
					);
					$this->session->set_userdata($member_data);
				}
				$this->session->set_userdata('logged_in', TRUE);
				redirect(base_url('dashboard'));
			} else {
				$this->session->set_flashdata('msg','Username & Password tidak cocok atau ID Belum Aktif, segera Hubungi Admin!');
				redirect(base_url('auth/login'));
			}
		}
	}

    public function activation($token){
    	if ($this->Auth_Model->activation($token)) {
    		// successfully sent mail
            $this->session->set_flashdata('msg','Your Email Address is successfully verified! Please login to access your account!');
            $this->load->view('login/success');
    	} else {
    		// error
            $this->session->set_flashdata('msg','Sorry! There is error verifying your Email Address or your Account is already Activated!');
            $this->load->view('login/success');
    	}
    }
    
}
