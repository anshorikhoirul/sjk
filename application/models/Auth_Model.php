<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_Model extends CI_Model{
	public function __construct(){
            parent::__construct();
    }

	public function login(){
		$email	= html_escape($this->input->post('email'));
		$pswd	= md5(html_escape($this->input->post('password')));
		$data	= array(
			'email'	=> $email,
			'password' => $pswd,
			'status' => 1
			);
		$this->db->select('email, role');
		$query	= $this->db->get_where('member', $data);
		if ($query->num_rows() == 1){
			// return TRUE;
			return $query->result()[0];
		} else {
			return FALSE;
		}
	}

	public function register($token){
		$id_member = null;
		if ($this->input->post('jenisuser') == 'siswa') {
			$id_siswa = null;
			$inputmember = array(
				'id_member' => $id_member,
				'email' => html_escape($this->input->post('email')),
				'password' => md5(html_escape($this->input->post('password'))),
				'role' => 2,
				'status' => 0,
				'token' => $token
			);
			$inputsiswa = array(
				'id_siswa' => $id_siswa,
				'nama_lengkap' => html_escape($this->input->post('fullname')),
				'email' => html_escape($this->input->post('email')),
				'tmpt_lhr' => html_escape($this->input->post('placeofbirth')),
				'tgl_lhr' => $this->input->post('dateofbirth'),
				'jk' => $this->input->post('gender'),
				'telp' => html_escape($this->input->post('phonenumber')),
				'profilpict' => ''
			);

			$query1 = $this->db->insert('member', $inputmember);
			$query2 = $this->db->insert('siswa', $inputsiswa);
		}else{
			$id_guru = null;
			$inputmember = array(
				'id_member' => $id_member,
				'email' => html_escape($this->input->post('email')),
				'password' => md5(html_escape($this->input->post('password'))),
				'role' => 1,
				'status' => 0,
				'token' => $token
			);

			$inputguru = array(
				'id_guru' => $id_guru,
				'nama_lengkap' => html_escape($this->input->post('fullname')),
				'email' => html_escape($this->input->post('email')),
				'telp' => html_escape($this->input->post('phonenumber')),
				'profilpict' => ''
			);

			$query1 = $this->db->insert('member', $inputmember);
			$query2 = $this->db->insert('guru', $inputguru);
		}
		return ($query1 && $query2);
	}

	public function activation($token){
		$data['status'] = 1;
		$param = array(
			'status' => 0, 
			'token' => $token
		);
		$this->db->from('member');
        $this->db->where($param);
		if (!$this->db->count_all_results()) {
			return false;
		}
        $this->db->where($param);
        return $this->db->update('member', $data);
	}
}