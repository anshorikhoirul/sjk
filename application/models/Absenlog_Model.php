<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absenlog_Model extends CI_Model{
	public function __construct(){
        parent::__construct();
    }

    public function cek_tglabsen($idkelas, $tgl){
        $this->db->select('*');
        $this->db->from('absen_log');
        $this->db->join('joinkelas', 'absen_log.joinkelas_id = joinkelas.id');
        $this->db->where('joinkelas.id_kelas', $idkelas);
        $this->db->where('absen_log.date', $tgl);
        $query = $this->db->get();

        if ($query->num_rows() == 0){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function generate_absen($idkelas, $tgl, $jam){
        $param = array(
            'id_kelas' => $idkelas,
            'status' => 1,
        );
        $querycek = $this->db->get_where('joinkelas', $param);
        if ($querycek->num_rows() == 0) {
            return FALSE;
        } else {
            foreach ($querycek->result() as $dkelas){
                $data = array(
                    'id_log' => '',
                    'joinkelas_id' => $dkelas->id,
                    'status_absen' => 'B',
                    'date' => $tgl,
                    'time' => $jam,
                );
                $query = $this->db->insert('absen_log', $data);
            }
            return TRUE;
        }
    }

    public function logabsen_view($idkelas){
        $this->db->select('*');
        $this->db->from('absen_log');
        $this->db->join('joinkelas', 'absen_log.joinkelas_id = joinkelas.id');
        $this->db->where('joinkelas.id_kelas', $idkelas);
        $this->db->group_by('absen_log.date');
        $this->db->order_by('absen_log.date DESC');
        $query = $this->db->get();

        return $query;
    }

    public function hapus_absen($idkelas, $tgl){
        $param1['id_kelas'] = $idkelas;
        $qselect = $this->db->get_where('joinkelas', $param);

        foreach ($qselect->result() as $dkelas) {
            $param = array(
                'joinkelas_id' => $dkelas->id,
                'date' => $tgl,
            );
            $this->db->where($param);
            $del = $this->db->delete('absen_log');
        }
        return TRUE;
    }

    public function log_view($id_kelas, $tgl){
        $date = date("Y-m-d", $tgl);
        $this->db->select('*');
        $this->db->from('absen_log');
        $this->db->join('joinkelas', 'absen_log.joinkelas_id = joinkelas.id');
        $this->db->join('siswa', 'joinkelas.id_siswa = siswa.id_siswa');
        $this->db->where('joinkelas.id_kelas', $id_kelas);
        $this->db->where('absen_log.date', $date);
        $query = $this->db->get();

        return $query;
    }

		public function cek_idlog($id, $tgl_absensi){
			$param = array(
					'id_log' => $id,
					'date' => $tgl_absensi,
			);
			$querycek = $this->db->get_where('absen_log', $param);
			if ($querycek->num_rows() == 0) {
					return FALSE;
			} else {
					return TRUE;
			}
		}

		public function profilsiswa($id_log){
			$this->db->select('nama_lengkap');
			$this->db->from('siswa');
			$this->db->join('joinkelas', 'siswa.id_siswa = joinkelas.id_siswa');
			$this->db->join('absen_log', 'joinkelas.id = absen_log.joinkelas_id');
			$this->db->where('absen_log.id_log', $id_log);
			$query = $this->db->get();

			return $query;
		}

		public function update_status($id_log, $status_absen){
			$param['id_log'] = $id_log;
			$data['status_absen'] = $status_absen;

			$this->db->where($param);
			if ($this->db->update('absen_log', $data)) {
				return TRUE;
			}else {
				return FALSE;
			}
		}

		public function update_absent($id_log, $status_absen){
			$param['id_log'] = $id_log;
			$data['status_absen'] = $status_absen;
			$this->db->where($param);
			if ($this->db->update('absen_log', $data)) {
				return TRUE;
			}else {
				return FALSE;
			}
		}

		public function lht_dataabsen($id_joinkelas){
			$param['joinkelas_id'] = $id_joinkelas;
			$this->db->order_by('date', 'ASC');
			$query  = $this->db->get_where('absen_log', $param);
			return $query->result();
		}

		public function cek_absenharian($tgl_skrg, $id_siswa){
			$this->db->select('*');
			$this->db->from('absen_log');
			$this->db->join('joinkelas', 'absen_log.joinkelas_id = joinkelas.id');
			$this->db->join('kelas', 'joinkelas.id_kelas = kelas.id');
			$this->db->join('guru', 'kelas.id_guru = guru.id_guru');
			$this->db->where('date', $tgl_skrg);
			$this->db->where('id_siswa', $id_siswa);
			$this->db->order_by('absen_log.date DESC');
			$query = $this->db->get();
			return $query->result();
		}

		public function cek_listsiswa($id_kelas){
			$this->db->select('*');
			$this->db->from('absen_log');
			$this->db->join('joinkelas', 'absen_log.joinkelas_id = joinkelas.id');
			$this->db->join('siswa', 'joinkelas.id_siswa = siswa.id_siswa');
			$this->db->where('id_kelas', $id_kelas);
			$query = $this->db->get();
			if ($query->num_rows() == 0) {
					return TRUE;
			} else {
					return FALSE;
			}
		}

		public function get_listsiswa($id_kelas){
			$this->db->select('nama_lengkap, joinkelas_id');
			$this->db->from('absen_log');
			$this->db->join('joinkelas', 'absen_log.joinkelas_id = joinkelas.id');
			$this->db->join('siswa', 'joinkelas.id_siswa = siswa.id_siswa');
			$this->db->where('id_kelas', $id_kelas);
			$this->db->order_by('siswa.nama_lengkap ASC');
			$this->db->group_by('joinkelas_id');
			$query = $this->db->get();
			return $query->result();
		}

		public function get_status($joinkelas_id, $status_absen){
			$param = array(
				'joinkelas_id' => $joinkelas_id,
				'status_absen' => $status_absen,
			);
			$this->db->select('status_absen');
			$this->db->from('absen_log');
			$this->db->where($param);
			$query = $this->db->get();
			return $query->num_rows();
		}

}
