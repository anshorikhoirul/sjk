<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_Model extends CI_Model{
	public function __construct(){
            parent::__construct();
    }

	public function config(){
		$data = array(
    		'id' => 1,
    		'type' => 'email_verification'
    	);
    	$query  = $this->db->get_where('email_activation', $data);
    	// $query = $this->db->get('email_activation');
		if ($query->num_rows() == 1){
			// return TRUE;
			return $query->row();
		} else {
			return FALSE;
		}
	}

	
}