<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_Model extends CI_Model{
	public function __construct(){
        parent::__construct();
    }

    public function tampil_data($id_guru){
    	$data['id_guru'] = $id_guru;
    	$this->db->order_by('ta DESC, semester DESC, kelas ASC, nama_mapel ASC');
		return $this->db->get_where('kelas', $data);
	}

    public function tambah_kelas($data){
    	$query = $this->db->insert('kelas', $data);
    	if ($query) {
    		return TRUE;
    	} else{
    		return FALSE;
    	}
    }

    public function cek_kelasguru($id_kelas, $id_guru){
    	$data = array(
    		'id' => $id_kelas,
    		'id_guru' => $id_guru,
    	);
    	$query  = $this->db->get_where('kelas', $data);
        if ($query->num_rows() == 1){
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function cek_kelassiswa($id_kelas, $id_siswa){
        $data = array(
            'id_kelas' => $id_kelas,
            'id_siswa' => $id_siswa
        );
        $query  = $this->db->get_where('joinkelas', $data);
        if ($query->num_rows() == 1){
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function cek_joinkelas($id_kelas){
        $data['id_kelas'] = $id_kelas;
        $query  = $this->db->get_where('joinkelas', $data);
        if ($query->num_rows() > 0){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function hapus_kelas($id_kelas){
        $this->db->where('id', $id_kelas);
        $query = $this->db->delete('kelas');
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function tampil_kelassiswa($id_siswa){
        $this->db->select('*');
        $this->db->from('kelas');
        $this->db->join('joinkelas', 'kelas.id = joinkelas.id_kelas');
        $this->db->where('id_siswa', $id_siswa);
        $this->db->order_by('kelas.ta DESC, kelas.semester DESC, kelas.kelas ASC, kelas.nama_mapel ASC');
        $query = $this->db->get();
        return $query;
    }

    public function cek_kode($kode_kelas){
        $data['kode_unik'] = $kode_kelas;
        $query  = $this->db->get_where('kelas', $data);
        if ($query->num_rows() > 0){
            return $query;
        } else {
            return FALSE;
        }
    }

    public function cek_siswajoinkelas($id_siswa, $id_kelas) {
        $data = array(
            'id_kelas' => $id_kelas,
            'id_siswa' => $id_siswa
        );
        $query  = $this->db->get_where('joinkelas', $data);
        if ($query->num_rows() == 0){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function joinkelas($id_siswa, $id_kelas){
        $data = array(
            'id' => null,
            'id_kelas' => $id_kelas,
            'id_siswa' => $id_siswa,
            'status' => 0
        );
        $query = $this->db->insert('joinkelas', $data);
        if ($query) {
            return TRUE;
        } else{
            return FALSE;
        }
    }

    public function periksa_joinkelas($id, $id_siswa){
        $data = array(
            'id' => $id,
            'id_siswa' => $id_siswa
        );
        $query  = $this->db->get_where('joinkelas', $data);
        if ($query->num_rows() == 0){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function approve_join($id_kelas, $id_siswa){
        $data = array('status' => 1);
        $param = array(
            'id_kelas' => $id_kelas,
            'id_siswa' => $id_siswa,
        );
        $this->db->where($param);
        return $this->db->update('joinkelas', $data);
    }

    public function reject_join($id_kelas, $id_siswa){
        $data = array('status' => 2);
        $param = array(
            'id_kelas' => $id_kelas,
            'id_siswa' => $id_siswa,
        );
        $this->db->where($param);
        return $this->db->update('joinkelas', $data);
    }

    public function dismiss_join($id_kelas, $id_siswa){
        $param = array(
            'id_kelas' => $id_kelas,
            'id_siswa' => $id_siswa
        );
        $this->db->where($param);
        return $query = $this->db->delete('joinkelas');
    }

    public function hapus_joinkelas($id){
        $this->db->where('id', $id);
        $query = $this->db->delete('joinkelas');
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function tampil_kelasguru($id_kelas){
        $this->db->select('*');
        $this->db->from('siswa');
        $this->db->join('joinkelas', 'siswa.id_siswa = joinkelas.id_siswa');
        $this->db->where('id_kelas', $id_kelas);
        // $this->db->order_by('kelas.ta DESC, kelas.semester DESC, kelas.kelas ASC, kelas.nama_mapel ASC');
        $query = $this->db->get();
        return $query;
    }

    public function ganti_status($idkelas, $new_status){
        $data['absen_status'] = $new_status;
        $param ['id'] = $idkelas;
        $this->db->where($param);
        return $this->db->update('kelas', $data);
    }

		public function cek_absenstatus($id_kelas){
			$data = array(
					'id' => $id_kelas,
					'absen_status' => 1
			);
			$query  = $this->db->get_where('kelas', $data);
			if ($query->num_rows() == 1){
					return TRUE;
			} else {
					return FALSE;
			}
		}
}
