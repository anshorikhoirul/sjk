<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model{
	public function __construct(){
        parent::__construct();
    }

    public function memberdata(){
    	$data['email'] = $this->session->userdata('email');
    	$role = $this->session->userdata('role');
    	if ($role == 1) {
    		$query = $this->db->get_where('guru', $data);
    	} else if ($role == 2) {
    		$query = $this->db->get_where('siswa', $data);
    	}
    	return $query;
    }

    public function profile_update($id, $data){
        $role = $this->session->userdata('role');
        if ($role == 1) {
            $this->db->where('id_guru', $id);
            if ($this->db->update('guru', $data)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else if ($role == 2) {
            $this->db->where('id_siswa', $id);
            if ($this->db->update('siswa', $data)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function cek_member($datamember){
        $this->db->select('email');
        $query  = $this->db->get_where('member', $datamember);
        if ($query->num_rows() == 1){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function setting_update($email, $data){
        $this->db->where('email', $email);
        $query = $this->db->update('member', $data);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

		public function lht_profilsiswa($id_siswa){
			$param['id_siswa'] = $id_siswa;
			$query = $this->db->get_where('siswa', $param);
			return $query;
		}

		public function lht_profilgurumapel($id_kelas){
			$this->db->select('*');
			$this->db->from('kelas');
			$this->db->join('guru', 'kelas.id_guru = guru.id_guru');
			$this->db->where('id', $id_kelas);
			$query = $this->db->get();
			return $query->row();
		}
}
