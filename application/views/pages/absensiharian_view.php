<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <?php 
      if ($this->session->flashdata()) { ?>
        <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-info"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
      <?php } ?>
      <div class="box box-success">
        <?php
          if ($role == 1) { ?>
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Kelas</h3>
            </div>
            <div class="box-body">
              <table id="datatbl1" class="table table-responsive table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="5%">#</th>
                    <th width="8%">GID</th>
                    <th width="30%">Mapel</th>
                    <th>Kelas</th>
                    <th>TA</th>
                    <th>Semester</th>
                    <th width="11%">Status Absen</th>
                    <th width="15%">Opsi</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $no = 1;
                foreach($dkelas as $datakelas){ ?>
                  <tr>
                    <td><?php echo $no++ ?></td>
                    <td><kbd><?php echo $datakelas->kode_unik ?></kbd></td>
                    <td><?php echo $datakelas->nama_mapel ?></td>
                    <td><?php echo $datakelas->kelas ?></td>
                    <td><?php echo $datakelas->ta ?></td>
                    <td><?php echo $datakelas->semester == 1?'Gasal':'Genap'; ?></td>
                    <td><?php echo $datakelas->absen_status == 0?'<span class="label label-default">Absensi Tidak Aktif</span>':'<span class="label label-success">Absensi Aktif</span>'; ?></td>
                    <td>
                      <a class="btn btn-warning btn-xs btn-flat" href="<?php echo base_url('absensi/ganti_status/'.$datakelas->id);?>">Ganti Status</a>
                      <a class="btn btn-primary btn-xs btn-flat" href="<?php echo base_url('absensi/absensi_harian/'.$datakelas->id);?>">Log Absen</a> 
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          <?php } elseif ($role == 2){
            echo "Halaman ABSENSI HARIAN untuk Siswa belum dibuat";
          }
        ?>
        
      </div>
    </div>
    <!-- /.col -->
  </div>
</section>