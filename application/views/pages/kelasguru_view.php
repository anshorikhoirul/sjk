<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <?php if ($this->session->flashdata()) { ?>
        <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-info"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
      <?php } ?>
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Siswa - <?php echo $nama_mapel;?></h3>
        </div>
        <div class="box-body">
          <table id="datatbl1" class="table table-responsive table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama Lengkap</th>
                <th>e-Mail</th>
                <th>Jenis Kelamin</th>
                <th>Status</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              foreach($dsiswa as $datasiswa){ ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $datasiswa->nama_lengkap ?></td>
                  <td><?php echo $datasiswa->email ?></td>
                  <td>
                    <?php if ($datasiswa->jk == 1) {
                      echo "Laki-laki";
                    } else if ($datasiswa->jk == 2) {
                      echo "Perempuan";
                    } ?>
                  </td>
                  <td>
                    <?php if ($datasiswa->status == 0) { ?>
                      <span class="label label-info">Menunggu</span>
                    <?php } else if ($datasiswa->status == 1) { ?>
                      <span class="label label-success">Diterima</span>
                    <?php } else { ?>
                      <span class="label label-warning">Ditolak</span>
                    <?php } ?>
                  </td>
                  <td>
                    <a href="<?php echo base_url('kelas/kelas_guru/'.$id_kelas.'/'.$datasiswa->id_siswa);?>" class="btn btn-info btn-xs btn-flat" data-toggle='tooltip' title="Lihat Data Siswa!"><span class="fa fa-user"></span></a>
                    <a href="<?php echo base_url('kelas/approve/'.$id_kelas.'/'.$datasiswa->id_siswa);?>" class="btn btn-success btn-xs btn-flat" data-toggle='tooltip' title="Ijinkan Akses!"><span class="fa fa-check"></span></a>
                    <a href="<?php echo base_url('kelas/reject/'.$id_kelas.'/'.$datasiswa->id_siswa);?>" class="btn btn-warning btn-xs btn-flat" data-toggle='tooltip' title="Tolak Akses!"><span class="fa fa-times"></span></a>
                    <a class="btn btn-danger btn-xs btn-flat el-del-btn" data-placement="bottom" data-link="<?php echo base_url('kelas/dismiss/'.$id_kelas.'/'.$datasiswa->id_siswa);?>" data-target="#delete-modal" data-toggle='modal'  title="Keluarkan Siswa!"><span class="fa fa-unlink"></span></a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content
