<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <?php if ($this->session->flashdata()) { ?>
        <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-info-circle"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
      <?php } ?>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Mata Pelajaran <?php echo $dkelas->nama_mapel; ?></h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <table class="table">
                <tr>
                  <th style="width:30%">Kode / Mapel</th>
                  <td><?php echo $dkelas->kode_unik.' / '.$dkelas->nama_mapel; ?></td>
                </tr>
                <tr>
                  <th>Kelas</th>
                  <td><?php echo $dkelas->kelas; ?></td>
                </tr>
              </table>
            </div>
            <div class="col-md-6">
              <table class="table">
                <tr>
                  <th style="width:30%">Tahun Ajaran</th>
                  <td><?php echo $dkelas->ta; ?></td>
                </tr>
                <tr>
                  <th>Semester</th>
                  <td><?php echo $dkelas->semester; ?></td>
                </tr>
              </table>
            </div>
          </div>
          <table id="datatbl1" class="table table-responsive table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th rowspan="2" width="5%" style="vertical-align:middle; text-align:center;">#</th>
                <th rowspan="2" width="30%" style="vertical-align:middle; text-align:center;">Nama Siswa</th>
                <th colspan="4" width="40%" style="vertical-align:middle; text-align:center;">Absensi</th>
                <th rowspan="2" style="vertical-align:middle; text-align:center;">Jumlah Absensi</th>
                <th rowspan="2" style="vertical-align:middle; text-align:center;">Persentase Kehadiran</th>
              </tr>
              <tr>
                <th width="10%" style="vertical-align:middle; text-align:center;">Sakit</th>
                <th width="10%" style="vertical-align:middle; text-align:center;">Ijin</th>
                <th width="10%" style="vertical-align:middle; text-align:center;">Alpha</th>
                <th width="10%" style="vertical-align:middle; text-align:center;">Masuk</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                for ($i=0; $i < $counter;) { ?>
                <tr>
                  <td style="vertical-align:middle; text-align:center;"><?php echo $no++; ?></td>
                  <td style="vertical-align:middle; text-align:left;"><?php echo $listsiswa[$i]; ?></td>
                  <td style="vertical-align:middle; text-align:center;"><?php echo $countS[$i]; ?></td>
                  <td style="vertical-align:middle; text-align:center;"><?php echo $countI[$i]; ?></td>
                  <td style="vertical-align:middle; text-align:center;"><?php echo $countA[$i]; ?></td>
                  <td style="vertical-align:middle; text-align:center;"><?php echo $countM[$i]; ?></td>
                  <td style="vertical-align:middle; text-align:center;"><?php echo $countJA[$i]; ?></td>
                  <td style="vertical-align:middle; text-align:center;"><?php echo $countPK[$i].' %'; ?></td>
                </tr>
                <?php
                $i++;
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
