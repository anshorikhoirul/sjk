<?php
    date_default_timezone_set("Asia/Jakarta");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SJK | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css')?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/flat/blue.css'); ?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css'); ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker.css'); ?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">
    <!-- Pace style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/pace/pace.min.css');?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini fixed">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url('dashboard');?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>SJK</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Sistem Jurnal Kelas</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <?php
                    if (empty($this->session->userdata('profilpict'))) { ?>
                      <img src="<?php echo base_url('assets/profilpict/unknown.png'); ?>" class="user-image" alt="User Image">
                    <?php } else { ?>
                      <img src="<?php echo base_url('assets/profilpict/'.$this->session->userdata('profilpict')); ?>" class="user-image" alt="User Image">
                    <?php }
                  ?>
                  <span class="hidden-xs"><?php echo $this->session->userdata('nama_lengkap'); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php
                      if (empty($this->session->userdata('profilpict'))) { ?>
                        <img src="<?php echo base_url('assets/profilpict/unknown.png'); ?>" class="img-circle" alt="User Image">
                      <?php } else { ?>
                        <img src="<?php echo base_url('assets/profilpict/'.$this->session->userdata('profilpict')); ?>" class="img-circle" alt="User Image">
                      <?php }
                    ?>
                    <p>
                      <?php echo $this->session->userdata('nama_lengkap'); ?> - <?php $role = $this->session->userdata('role'); echo $role == 1?'Guru':'Siswa'; ?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url('profile');?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url('logout');?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <?php
                if (empty($this->session->userdata('profilpict'))) { ?>
                  <img src="<?php echo base_url('assets/profilpict/unknown.png'); ?>" class="img-circle" alt="User Image">
                <?php } else { ?>
                  <img src="<?php echo base_url('assets/profilpict/'.$this->session->userdata('profilpict')); ?>" class="img-circle" alt="User Image">
                <?php }
              ?>
            </div>
            <div class="pull-left info">
              <p><?php echo $this->session->userdata('nama_lengkap'); ?></p>
              <a href="#"><i class="fa fa-user fa-lg text-success"></i> <?php $role = $this->session->userdata('role'); echo $role == 1?'Guru':'Siswa'; ?></a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                  </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?php echo base_url('dashboard');?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('kelas');?>">
                <i class="fa fa-group"></i> <span>Kelas</span>
              </a>
            </li>
            <?php
              if ($this->session->userdata('role') == 1) { ?>
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-check-circle-o"></i> <span>Absensi</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('absensi/absensi_harian');?>"><i class="fa fa-circle-o"></i> Absensi Harian</a></li>
                    <li><a href="<?php echo base_url('absensi/rekap_absensi');?>"><i class="fa fa-circle-o"></i> Rekap Absensi</a></li>
                  </ul>
                </li>
              <?php } else { ?>
                <li>
                  <a href="<?php echo base_url('absensi/absensi_harian');?>">
                    <i class="fa fa-check-circle-o"></i> <span> Absensi Harian</span>
                  </a>
                </li>
              <?php } ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php
            $last = $this->uri->total_segments();
            if ($last > 2) {
              $record_num = $this->uri->segment(2);
            } else {
              $record_num = $this->uri->segment($last);
            }
            $judul = str_replace('_', ' ', $record_num);
            echo ucwords($judul);
            ?>
            <small>Page</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
            <?php
            $segs = $this->uri->segment_array();
            $k = 0;
            foreach ($segs as $segment) {
              if ($k == 2) {
                break;
              } else {
                $k++;
              }
                $breadcrumburl = str_replace('_', ' ', $segment);
                echo '<li><a href="#">'.ucwords($breadcrumburl).'</a></li>';
            }
            ?>
          </ol>
        </section>

        <!-- Main content -->
        <?php $this->load->view('pages/'.$page.'_view'); ?>
        <!-- /.content -->
        <div class="modal fade modal-super-scaled" id="delete-modal" aria-hidden="true" aria-labelledby="Delete Modal" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-sm">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="icon fa fa-info"></i> Hapus Data</h4>
                        </div>
                        <div class="panel-body">
                            Apakah anda yakin ingin menghapusnya?
                        </div>
                        <div class="panel-footer">
                            <button type="button" class="btn btn-default btn-sm margin-0" data-dismiss="modal">Close</button>
                            <a id="change-del-btn" href="" type="button" class="btn btn-sm btn-danger">Hapus</a>
                        </div>
                    </div>

                </div>
            </div>
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.7
        </div>
        <strong>Copyright &copy; 2016-2017 <a href="http://tretesonline.com" target="_blank">TretesOnline Studio</a>.</strong> All rights
          reserved.
      </footer>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo base_url('assets/plugins/knob/jquery.knob.js'); ?>"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
    <!-- PACE -->
    <script src="<?php echo base_url('assets/plugins/pace/pace.min.js');?>"></script>
    <script type="text/javascript">
      $(document).ajaxStart(function() { Pace.restart(); });
    </script>
    <!-- Validasi Inputan Angka-->
    <script type="text/javascript" src="<?php echo base_url('assets/validator_angka.js');?>"></script>
    <!-- Active Class for Sidebar-Menu -->
    <script src="<?php echo base_url('assets/custom.js'); ?>"></script>
    <!-- DataTables -->
    <script>
      $(function () {
        $("#datatbl1").DataTable({"scrollX": true, "pageLength": 25 });
        $('#datatbl2').DataTable({
          "paging": true,
          "responsive": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "scrollX": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
    <!-- Notif Delete Modal -->
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $(".el-del-btn").click(function() {
            var link = $(this).attr('data-link');
            var btnChange = $('#change-del-btn');
            btnChange.attr("href", link);
        });
        var id_link = '#<?= isset($active_link) ? $active_link : NULL ?>';

        if(id_link != "#"){
            $(id_link).addClass('active');
            $(id_link).closest('ul').addClass("in");
            $(id_link).closest('ul').closest('li').addClass('active');
        }
        $('form').validator().on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...

                $('html, body').animate({ scrollTop: $(".has-error:first :input").offset().top - 100 }, 50);
                $(".has-error:first :input").focus();

            } else {
                // everything looks good!
            }
        });
    });
    </script>
  </body>
</html>
