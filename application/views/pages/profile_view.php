<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-3">
      <!-- Profile Image -->
      <div class="box box-info">
        <div class="box-body box-profile">
          <?php
            if (empty($this->session->userdata('profilpict'))) { ?>
              <img src="<?php echo base_url('assets/profilpict/unknown.png'); ?>" class="profile-user-img img-responsive img-circle" alt="User profile picture">
            <?php } else { ?>
              <img src="<?php echo base_url('assets/profilpict/'.$this->session->userdata('profilpict')); ?>" class="profile-user-img img-responsive img-circle" alt="User profile picture">
            <?php }
          ?>
          <h3 class="profile-username text-center"><?php echo $this->session->userdata('nama_lengkap'); ?></h3>
          <p class="text-muted text-center"><?php $role = $this->session->userdata('role'); echo $role == 1?'Guru':'Siswa'; ?></p>
          <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('profile/update_photo');?>">
          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                  <input type="file" name="photoprofile" id="photoprofile" required>
            </li>
          </ul>
          <button type="submit" class="btn btn-danger btn-block"><b>Update Photo</b></button>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <?php 
      if ($this->session->flashdata()) { ?>
        <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-info"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
      <?php } ?>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">User Profile </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="form-horizontal" method="post" action="<?php echo base_url('profile/update_profile');?>">
              <div class="form-group hidden">
                <label for="id" class="col-sm-2 control-label">Id</label>
                <div class="col-sm-8">
                  <input type="number" class="form-control" name="id" id="id" value="<?php echo $id;?>" placeholder="id" readonly>
                </div>
              </div>
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nama</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="inputName" id="inputName" value="<?php echo $nama_lengkap;?>" placeholder="Nama" readonly>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="email" class="form-control" name="inputEmail" id="inputEmail" value="<?php echo $email;?>" placeholder="Email" readonly>
                </div>
              </div>
              <?php
              if ($role == 2) { ?>
                <div class="form-group">
                <label for="inputTmptLhr" class="col-sm-2 control-label">Tempat Lahir</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="inputTmptLhr" id="inputTmptLhr" value="<?php echo $tmpt_lhr;?>" placeholder="Tempat Lahir" readonly>
                </div>
              </div>
              <div class="form-group">
                <label for="inputTglLhr" class="col-sm-2 control-label">Tanggal Lahir</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="inputTglLhr" id="inputTglLhr" value="<?php echo $tgl_lhr;?>" placeholder="Tanggal Lahir" readonly>
                </div>
              </div>
              <div class="form-group">
                <label for="inputJK" class="col-sm-2 control-label">Jenis Kelamin</label>
                <div class="col-sm-8">
                  <select class="form-control" name="inputJK" id="inputJK" readonly>
                    <?php
                    if ($jk == 1) { ?>
                      <option value="1">Laki-laki</option>
                    <?php } else { ?>
                      <option value="2">Perempuan</option>
                    <?php }
                    ?>
                  </select>
                </div>
              </div>
              <?php }
              ?>
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">No Telp</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control numberonly" maxlength="13" name="inputPhone" id="inputPhone" value="<?php echo $notelp;?>" placeholder="Nomor Telepon" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-danger">Simpan</button>
                </div>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Login Setting </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="form-horizontal" method="post" action="<?php echo base_url('profile/update_pass');?>">
              <div class="form-group" hidden>
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="inputEmail" id="inputEmail" value="<?php echo $email;?>" placeholder="Email" readonly>
                </div>
              </div>
              <div class="form-group">
                <label for="inputOldPass" class="col-sm-2 control-label">Old Pass</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="inputOldPass" id="inputOldPass" placeholder="Old Password" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputNewPass" class="col-sm-2 control-label">New Pass</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="inputNewPass" id="inputNewPass" placeholder="New Password" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputConfPass" class="col-sm-2 control-label">Confirm Pass</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="inputConfPass" id="inputConfPass" placeholder="Retype New Password" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-danger">Simpan</button>
                </div>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->