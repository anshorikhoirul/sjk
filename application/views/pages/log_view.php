<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <?php if ($this->session->flashdata()) { ?>
        <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-info-circle"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
      <?php } ?>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Log Absensi | <?php echo $kelasmapel.' | '.$tglabsensi; ?></h3>
        </div>
        <div class="box-body">
          <table id="datatbl1" class="table table-responsive table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Foto</th>
                <th>Data Siswa</th>
                <th style="text-align: center">Keterangan</th>
                <th style="text-align: center">Opsi Absensi</th>
              </tr>
            </thead>
            <tbody>
            <?php
              $no = 1;
              $tglskrg = date("Y-m-d");
              foreach ($dabsen as $dataabsensi) {
                $tgllhr = date("d-m-Y", strtotime($dataabsensi->tgl_lhr));

                ?>
                <tr>
                  <td width="5%" align="center"><h4><?php echo $no++; ?></h4></td>
                  <td width="12%">
                    <?php
                    if (empty($dataabsensi->profilpict)) { ?>
                      <img width="113px" height="151px"  src="<?php echo base_url('assets/profilpict/unknown.png'); ?>" class="img-thumbnail">
                    <?php } else { ?>
                      <img width="113px" height="151px" src="<?php echo base_url('assets/profilpict/'.$dataabsensi->profilpict); ?>" class="img-thumbnail">
                    <?php }
                  ?>
                  </td>
                  <td width="40%">
                    <h4><?php echo $dataabsensi->nama_lengkap; ?></h4>
                    <p><span class="fa fa-venus-mars"></span> <?php echo $dataabsensi->jk == 1?'Laki-laki':'Perempuan'; ?></p>
                    <p><span class="fa fa-calendar"></span> <?php echo $dataabsensi->tmpt_lhr.', '.$tgllhr; ?></p>
                    <p><span class="fa fa-paper-plane"></span> <?php echo $dataabsensi->email; ?></p>
                    <p><span class="fa fa-phone-square"></span> <?php echo $dataabsensi->telp; ?></p>
                  </td>
                  <td width="20%" align="center"><h4>
                  <?php
                    if ($dataabsensi->status_absen == 'M') {
                      echo "Masuk";
                    } elseif ($dataabsensi->status_absen == 'S') {
                      echo "Sakit";
                    } elseif ($dataabsensi->status_absen == 'I') {
                      echo "Izin";
                    } elseif ($dataabsensi->status_absen == 'A') {
                      echo "Tidak Masuk <br/>Tanpa Keterangan";
                    } else {
                      echo "Belum Absen";
                    }
                  ?>
                  </h4></td>
                  <td align="center">
                    <?php
                      if ($dataabsensi->date == $tglskrg) { ?>
                        <h4>
                          <a class="btn btn-app" href="<?php echo base_url('absensi/setmasuk/'.$dataabsensi->id_kelas.'/'.strtotime($tglabsensi).'/'.$dataabsensi->id_log);?>">
                            <i class="fa fa-check-square-o"></i> Masuk
                          </a>
                          <a class="btn btn-app" href="<?php echo base_url('absensi/setabsent/'.$dataabsensi->id_kelas.'/'.strtotime($tglabsensi).'/'.$dataabsensi->id_log).'/s';?>">
                            <i class="fa fa-medkit"></i> Sakit
                          </a>
                        </h4>
                        <h4>
                          <a class="btn btn-app" href="<?php echo base_url('absensi/setabsent/'.$dataabsensi->id_kelas.'/'.strtotime($tglabsensi).'/'.$dataabsensi->id_log).'/i';?>">
                            <i class="fa fa-edit"></i> Ijin
                          </a>
                          <a class="btn btn-app" href="<?php echo base_url('absensi/setabsent/'.$dataabsensi->id_kelas.'/'.strtotime($tglabsensi).'/'.$dataabsensi->id_log).'/a';?>">
                            <i class="fa fa-user-times"></i> Alpha
                          </a>
                        </h4>
                      <?php } else {?>
                        <h4>Masa Absensi Sudah Lewat</h4>
                      <?php }
                    ?>
                  </td>
                </tr>
              <?php }
            ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<!--
<img style="object-fit: cover; width:113px; height:151px;"  class="media-object img-rounded img-responsive"  src="http://placehold.it/350x250" alt="placehold.it/350x250" >
 -->
