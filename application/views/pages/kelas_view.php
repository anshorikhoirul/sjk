<?php
  $role = $this->session->userdata('role');
?>
<!-- Main content -->
<section class="content">
  <div class="row">
  <?php if ($role == 1) { ?>
    <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Kelas</h3>
        </div>
        <div class="box-body no-padding">
          <form role="form" method="post" action="<?php echo base_url('kelas/tambah_kelas');?>">
            <div class="box-body">
              <div class="form-group">
                <label for="nama-mapel">Nama Matapelajaran</label>
                <input type="text" class="form-control" maxlength="20" name="nama-mapel" id="nama-mapel" value="<?php echo set_value('nama-mapel');?>" placeholder="Nama Matapelajaran" required>
                <?php echo form_error('nama-mapel'); ?>
              </div>
              <div class="form-group">
                <label for="kelas-mapel">Kelas</label>
                <input type="text" class="form-control" maxlength="15" name="kelas-mapel" id="kelas-mapel" value="<?php echo set_value('kelas-mapel');?>" placeholder="Kelas Matapelajaran" required>
                <?php echo form_error('kelas-mapel'); ?>
              </div>
              <div class="form-group">
                <label for="tahun-ajaran">Tahun Ajaran</label>
                <input type="text" class="form-control" maxlength="10" name="tahun-ajaran" id="tahun-ajaran" value="<?php echo set_value('tahun-ajaran');?>" placeholder="Tahun Ajaran" required>
                <?php echo form_error('tahun-ajaran'); ?>
              </div>
              <div class="form-group">
                <label for="semester">Semester</label>
                <select name="semester" id="semester" class="form-control" >
                  <option value="1">Gasal</option>
                  <option value="2">Genap</option>
                </select>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php } else { ?>
    <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Join Kelas</h3>
        </div>
        <div class="box-body no-padding">
          <form role="form" method="post" action="<?php echo base_url('kelas/joinkelas');?>">
            <div class="box-body">
              <div class="form-group">
                <label for="kode-kelas">Kode Kelas</label>
                <input type="text" class="form-control" name="kode-kelas" id="kode-kelas" maxlength="6" minlength="6" placeholder="Kode Kelas" required>
                <?php echo form_error('kode-kelas'); ?>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php } ?>
    <div class="col-md-9">
      <?php
      if ($this->session->flashdata()) { ?>
        <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-info"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
      <?php } ?>
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Kelas</h3>
        </div>
        <div class="box-body">
          <table id="datatbl1" class="table table-responsive table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>GID</th>
                <th>Mapel</th>
                <th>Kelas</th>
                <th>TA</th>
                <th>Semester</th>
                <?php echo $role == 2?'<th>Status</th>':''; ?>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>

            <?php
            $no = 1;
            foreach($dkelas as $datakelas){ ?>
              <tr>
                <td><?php echo $no++ ?></td>
                <td><kbd><?php echo $datakelas->kode_unik ?></kbd></td>
                <td><?php echo $datakelas->nama_mapel ?></td>
                <td><?php echo $datakelas->kelas ?></td>
                <td><?php echo $datakelas->ta ?></td>
                <td><?php echo $datakelas->semester == 1?'Gasal':'Genap'; ?></td>
                <?php
                  if($role == 2){
                    if ($datakelas->status == 0) { ?>
                      <td><span class="label label-info">Menunggu</span></td>
                    <?php } else if ($datakelas->status == 1) { ?>
                      <td><span class="label label-success">Diterima</span></td>
                    <?php } else { ?>
                      <td><span class="label label-warning">Ditolak</span></td>
                    <?php }
                  } ?>
                <td>
                <?php
                if ($role == 1) { ?>
                  <a class="btn btn-success btn-xs btn-flat" href="<?php echo base_url('kelas/kelas_guru/'.$datakelas->id);?>">Buka</a>
                  <a class="btn btn-danger btn-xs btn-flat el-del-btn" data-placement="bottom" data-link="<?php echo base_url('kelas/hapus_kelas/'.$datakelas->id);?>" data-target="#delete-modal" data-toggle="modal" title="Hapus Kelas">Hapus</a>
                <?php } else { ?>
                  <?php if ($datakelas->status == 1) { ?>
                    <a class="btn btn-success btn-xs btn-flat" href="<?php echo base_url('kelas/kelas_siswa/'.$datakelas->id_kelas);?>">Buka</a>
                  <?php } else { ?>
                    <a class="btn btn-danger btn-xs btn-flat el-del-btn" data-placement="bottom" data-link="<?php echo base_url('kelas/hapus_joinkelas/'.$datakelas->id_kelas);?>" data-target="#delete-modal" data-toggle="modal" title="Hapus Kelas" >Hapus</a>
                  <?php }
                } ?>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
