<!-- Main content -->
<section class="content">
  <?php if ($role == 1) { ?>
    <div class="row">
      <div class="col-md-8">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Data Kelas</h3>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th style="width:30%">Kode</th>
                  <td><?php echo $dkelas->kode_unik; ?></td>
                </tr>
                <tr>
                  <th>MataPelajaran</th>
                  <td><?php echo $dkelas->nama_mapel; ?></td>
                </tr>
                <tr>
                  <th>Kelas</th>
                  <td><?php echo $dkelas->kelas; ?></td>
                </tr>
                <tr>
                  <th>Tahun Ajaran</th>
                  <td><?php echo $dkelas->ta; ?></td>
                </tr>
                <tr>
                  <th>Semester</th>
                  <td><?php echo $dkelas->semester; ?></td>
                </tr>
                <tr>
                  <th>Status</th>
                  <td><?php echo $dkelas->absen_status == 1?'Absensi Aktif':'Absensi Tidak Aktif'; ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Absensi Baru</h3></div>
          <div class="box-body">
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
              <div class="info-box-content">
                <!-- <span class="info-box-text"></span> -->
                <span class="info-box-number" style="text-align: center;"><?php echo date('l, d M Y'); ?></span>
                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                  <a class="btn btn-xl bg-yellow btn-block" title="Klik untuk mulai absensi!" href="<?php echo base_url('absensi/create_absen/'.$dkelas->id);?>">Klik untuk mulai absensi!</a>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <?php if ($this->session->flashdata()) { ?>
          <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info-circle"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
              <?php echo $this->session->flashdata('msg'); ?>
          </div>
        <?php } ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Log Absensi</h3>
          </div>
          <div class="box-body">
            <table id="datatbl1" class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th>Tanggal Absensi</th>
                  <th>Keterangan</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $no = 1;
                  $lastdate = strtotime(date('d M Y'));
                  foreach ($dabsen as $dataabsensi) {
                    $tgl = date("d M Y", strtotime($dataabsensi->date));
                    $strtgl = strtotime($tgl);
                  ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $tgl; ?></td>
                      <td><?php echo $lastdate > $strtgl?'Selesai':'Dalam Proses'; ?></td>
                      <td>
                        <a href="<?php echo base_url('absensi/log/'.$dkelas->id.'/'.strtotime($dataabsensi->date));?>" class="btn btn-primary btn-sm btn-flat">Buka Absensi</a>
                        <a class="btn btn-danger btn-sm btn-flat el-del-btn" data-placement="bottom" data-link="<?php echo base_url('absensi/hapus_absen/'.$dkelas->id.'/'.strtotime($dataabsensi->date));?>" data-target="#delete-modal" data-toggle='modal'  title="Hapus Absensi!">Hapus Absensi</a>
                      </td>
                    </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  <?php } elseif ($role == 2){ ?>
      <?php if ($this->session->flashdata()) { ?>
        <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-info-circle"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
      <?php } ?>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Absensi Harian <?php echo date("d M Y");?></h3>
          </div>
          <div class="box-body">
            <?php
            if (empty($infomapel)) { ?>
              <blockquote>
                <p>Hari ini belum ada absensi untuk ditampilkan, silahkan refresh halaman atau periksa kembali setelah guru memulai absensi...!</p>
                <footer>ADMIN <cite title="Source Title">SJK</cite></footer>
              </blockquote>
            <?php } else {
              foreach ($infomapel as $dmapel) { ?>
                <div class="row">
                  <div class="col-md-2 hidden-sm hidden-xs">
                    <?php
                      if (empty($dmapel->profilpict)) { ?>
                        <img src="<?php echo base_url('assets/profilpict/unknown.png'); ?>" class="avatar img-thumbnail" alt="avatar">
                      <?php } else { ?>
                        <img src="<?php echo base_url('assets/profilpict/'.$dmapel->profilpict); ?>" class="avatar img-thumbnail" alt="avatar">
                      <?php }
                    ?>
                  </div>
                  <div class="col-md-5 col-xs-6">
                    <p class="lead">Informasi Matapelajaran</p>
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <th style="width:20%">Nama Guru</th>
                          <td><?php echo $dmapel->nama_lengkap; ?></td>
                        </tr>
                        <tr>
                          <th>Matapelajaran</th>
                          <td><?php echo $dmapel->nama_mapel; ?></td>
                        </tr>
                        <tr>
                          <th>Kelas/Semester</th>
                          <td><?php echo $dmapel->kelas.' / '.$dmapel->semester; ?></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="col-md-5 col-xs-6">
                    <p class="lead">Form Absensi</p>
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <th style="width:30%">Status</th>
                          <td>
                            <?php
                              if ($dmapel->absen_status == 1) {
                                echo "<span class='label label-primary'>Absensi Aktif</span>";
                              } else {
                                echo "<span class='label label-danger'>Absensi Tidak Aktif</span>";
                              }
                              echo " | ";
                              if ($dmapel->status_absen == 'M') {
                                echo "<span class='label label-success'>Masuk</span>";
                              } elseif ($dmapel->status_absen == 'S') {
                                echo "<span class='label label-warning'>Sakit</span>";
                              } elseif ($dmapel->status_absen == 'I') {
                                echo "<span class='label label-info'>Izin</span>";
                              } elseif ($dmapel->status_absen == 'A') {
                                echo "<span class='label label-danger'>Tidak Masuk Tanpa Keterangan</span>";
                              } else {
                                echo "<span class='label label-default'>Belum Absen</span>";
                              }
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <th>Last Update</th>
                          <td><?php echo $dmapel->time; ?></td>
                        </tr>
                        <tr>
                          <th></th>
                          <td>
                            <?php
                              if ($dmapel->status_absen == 'B') { ?>
                                <a class="btn btn-xs btn-success" href="<?php echo base_url('absensi/setmasuk/'.$dmapel->id_kelas.'/'.strtotime(date("d M Y")).'/'.$dmapel->id_log);?>">Cek Masuk</a>
                              <?php }
                            ?>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              <?php }
            } ?>




          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</section>
