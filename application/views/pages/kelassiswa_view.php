<!-- Main content -->
<section class="content">
  <?php if ($this->session->flashdata()) { ?>
    <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-info"></i> <?php echo ucfirst($this->session->flashdata('type')); ?>!</h4>
        <?php echo $this->session->flashdata('msg'); ?>
    </div>
  <?php } ?>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Informasi Matapelajaran</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
              <?php
                if (empty($infomapel->profilpict)) { ?>
                  <img src="<?php echo base_url('assets/profilpict/unknown.png'); ?>" class="avatar img-thumbnail" alt="avatar">
                <?php } else { ?>
                  <img src="<?php echo base_url('assets/profilpict/'.$infomapel->profilpict); ?>" class="avatar img-thumbnail" alt="avatar">
                <?php }
              ?>
            </div>
            <div class="col-md-9">
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th style="width:20%">Nama Guru</th>
                    <td><?php echo $infomapel->nama_lengkap;?></td>
                  </tr>
                  <tr>
                    <th>Nomor Telepon</th>
                    <td><?php echo $infomapel->telp;?></td>
                  </tr>
                  <tr>
                    <th>Matapeplajaran</th>
                    <td><?php echo $infomapel->nama_mapel;?></td>
                  </tr>
                  <tr>
                    <th>Kelas</th>
                    <td><?php echo $infomapel->kelas;?></td>
                  </tr>
                  <tr>
                    <th>Tahun Ajaran</th>
                    <td><?php echo $infomapel->ta;?></td>
                  </tr>
                  <tr>
                    <th>Semester</th>
                    <td><?php echo $infomapel->semester == 1?'Gasal':'Genap'; ?>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Absensi - <?php echo $infomapel->nama_mapel;?></h3>
        </div>
        <div class="box-body">
          <table id="datatbl1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th width="5%">No</th>
                <th>Tanggal Absensi</th>
                <th>Update Terakhir</th>
                <th>Status Absensi</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                foreach ($dabsen as $logabsen) { ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo date("d M Y", strtotime($logabsen->date)); ?></td>
                    <td><?php echo $logabsen->time; ?></td>
                    <td>
                      <?php
                        if ($logabsen->status_absen == 'M') {
                          echo "Masuk";
                        } elseif ($logabsen->status_absen == 'S') {
                          echo "Sakit";
                        } elseif ($logabsen->status_absen == 'I') {
                          echo "Izin";
                        } elseif ($logabsen->status_absen == 'A') {
                          echo "Tidak Masuk <br/>Tanpa Izin";
                        } else {
                          echo "Belum Absen";
                        }
                      ?>
                    </td>
                  </tr>
                <?php }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content
