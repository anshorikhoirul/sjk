<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="<?php echo base_url();?>"><b>Register</b>SJK</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <?php echo form_open(base_url().'auth/submit_register');?>
      <input type="hidden" name="jenisuser" value="<?php echo $jenisuser; ?>">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="fullname" value="<?php echo set_value('fullname');?>" placeholder="Full Name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        <?php echo form_error('fullname'); ?>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" value="<?php echo set_value('email');?>" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <?php echo form_error('email'); ?>
      </div>
      <?php
        if ($jenisuser == 'siswa') { ?>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="placeofbirth" value="<?php echo set_value('placeofbirth');?>" placeholder="Place of Birth" >
            <span class="fa fa-location-arrow form-control-feedback"></span>
            <?php echo form_error('placeofbirth'); ?>
          </div>
          <div class="form-group has-feedback">
            <input type="date" class="form-control" name="dateofbirth" value="<?php echo set_value('dateofbirth');?>" placeholder="Date of Birth">
            <span class="fa fa-calendar form-control-feedback"></span>
            <?php echo form_error('dateofbirth'); ?>
          </div>
          <div class="form-group has-feedback">
            <select class="form-control" name="gender" placeholder="Gender">
              <option value="">Gender</option>
              <option value="1">Male</option>
              <option value="2">Female</option>
            </select>
            <span class="fa fa-genderless form-control-feedback"></span>
            <?php echo form_error('gender'); ?>
          </div>
        <?php }
      ?>
      <div class="form-group has-feedback">
        <input type="text" class="numberonly form-control" maxlength="12" name="phonenumber" value="<?php echo set_value('phonenumber');?>" placeholder="Phone Number">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
        <?php echo form_error('phonenumber'); ?>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <?php echo form_error('password'); ?>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="passconf" placeholder="Password Confirmation">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        <?php echo form_error('passconf'); ?>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close();?>

    <a href="<?php echo base_url();?>auth/login" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<!-- Validasi Inputan Angka-->
<script type="text/javascript" src="<?php echo base_url();?>assets/validator_angka.js"></script>
</body>
</html>
