-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: sql109.rf.gd
-- Generation Time: Aug 29, 2017 at 10:43 PM
-- Server version: 5.6.35-81.0
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rfgd_19440595_sjk`
--

-- --------------------------------------------------------

--
-- Table structure for table `absen_log`
--

CREATE TABLE IF NOT EXISTS `absen_log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `joinkelas_id` int(11) NOT NULL,
  `status_absen` char(1) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `absen_log`
--

INSERT INTO `absen_log` (`id_log`, `joinkelas_id`, `status_absen`, `date`, `time`) VALUES
(29, 7, 'B', '2017-06-06', '10:39:55'),
(30, 8, 'B', '2017-06-06', '10:39:55'),
(31, 7, 'B', '2017-06-07', '09:16:10'),
(32, 8, 'B', '2017-06-07', '09:16:10'),
(33, 7, 'B', '2017-06-08', '09:23:42'),
(34, 8, 'B', '2017-06-08', '09:23:42'),
(35, 7, 'B', '2017-06-11', '23:19:25'),
(36, 8, 'B', '2017-06-11', '23:19:25'),
(37, 7, 'B', '2017-06-12', '05:27:42'),
(38, 8, 'B', '2017-06-12', '05:27:42'),
(39, 7, 'B', '2017-06-14', '11:46:34'),
(40, 8, 'B', '2017-06-14', '11:46:34'),
(41, 7, 'B', '2017-06-16', '13:14:00'),
(42, 8, 'B', '2017-06-16', '13:14:00'),
(43, 7, 'B', '2017-06-17', '11:44:25'),
(44, 8, 'B', '2017-06-17', '11:44:25'),
(45, 7, 'M', '2017-06-21', '08:21:15'),
(46, 8, 'M', '2017-06-21', '08:21:15'),
(47, 7, 'M', '2017-06-23', '22:24:41'),
(48, 8, 'B', '2017-06-23', '22:24:41'),
(49, 7, 'S', '2017-06-24', '04:25:42'),
(50, 8, 'I', '2017-06-24', '04:25:42'),
(51, 7, 'M', '2017-06-25', '17:11:06'),
(52, 8, 'A', '2017-06-25', '17:11:06'),
(53, 7, 'B', '2017-06-27', '07:49:34'),
(54, 8, 'B', '2017-06-27', '07:49:34'),
(55, 7, 'B', '2017-06-30', '23:25:12'),
(56, 8, 'B', '2017-06-30', '23:25:12'),
(57, 7, 'I', '2017-07-07', '19:36:21'),
(58, 8, 'S', '2017-07-07', '19:36:21'),
(59, 7, 'M', '2017-07-15', '12:20:32'),
(60, 8, 'B', '2017-07-15', '12:20:32'),
(64, 9, 'B', '2017-08-05', '14:17:05'),
(65, 10, 'B', '2017-08-05', '14:17:05'),
(66, 7, 'B', '2017-08-05', '14:18:18'),
(67, 8, 'B', '2017-08-05', '14:18:18'),
(68, 7, 'B', '2017-08-06', '16:06:43'),
(69, 8, 'B', '2017-08-06', '16:06:43'),
(70, 9, 'B', '2017-08-06', '16:06:50'),
(71, 10, 'B', '2017-08-06', '16:06:50'),
(72, 7, 'B', '2017-08-07', '21:49:24'),
(73, 8, 'B', '2017-08-07', '21:49:24'),
(74, 9, 'B', '2017-08-07', '21:49:36'),
(75, 10, 'B', '2017-08-07', '21:49:36'),
(76, 7, 'M', '2017-08-10', '08:48:55'),
(77, 8, 'A', '2017-08-10', '08:48:55'),
(78, 9, 'B', '2017-08-10', '08:48:57'),
(79, 10, 'I', '2017-08-10', '08:48:57');

-- --------------------------------------------------------

--
-- Table structure for table `email_activation`
--

CREATE TABLE IF NOT EXISTS `email_activation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `protocol` char(20) NOT NULL,
  `smtp_crypto` char(5) NOT NULL,
  `smtp_host` varchar(50) NOT NULL,
  `smtp_port` int(10) NOT NULL,
  `smtp_user` varchar(50) NOT NULL,
  `smtp_pass` varchar(50) NOT NULL,
  `mailtype` char(10) NOT NULL,
  `charset` varchar(20) NOT NULL,
  `email_name` varchar(50) NOT NULL,
  `email_subject` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `email_activation`
--

INSERT INTO `email_activation` (`id`, `type`, `protocol`, `smtp_crypto`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `mailtype`, `charset`, `email_name`, `email_subject`) VALUES
(1, 'email_verification', 'smtp', 'ssl', 'smtp.googlemail.com', 465, 'devtretesonline@gmail.com', 'QazWsx123456', 'html', 'iso-8859-1', 'SMAN 1 Pandaan', 'Activation Code For SJK System');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `id_guru` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `profilpict` varchar(35) NOT NULL,
  PRIMARY KEY (`id_guru`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `nama_lengkap`, `email`, `telp`, `profilpict`) VALUES
(1, 'Khoirul Anshori', 'testerguru@gmail.com', '085230240472', '');

-- --------------------------------------------------------

--
-- Table structure for table `joinkelas`
--

CREATE TABLE IF NOT EXISTS `joinkelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `joinkelas`
--

INSERT INTO `joinkelas` (`id`, `id_kelas`, `id_siswa`, `status`) VALUES
(7, 2, 2, 1),
(8, 2, 50, 1),
(9, 4, 50, 1),
(10, 4, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_guru` int(11) NOT NULL,
  `kode_unik` varchar(6) NOT NULL,
  `nama_mapel` varchar(20) NOT NULL,
  `kelas` varchar(15) NOT NULL,
  `ta` varchar(10) NOT NULL,
  `semester` int(1) NOT NULL,
  `absen_status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_unik` (`kode_unik`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `id_guru`, `kode_unik`, `nama_mapel`, `kelas`, `ta`, `semester`, `absen_status`) VALUES
(2, 1, 'dd3b22', 'Bahasa Indonesia', '12 IPA 5', '2016/2017', 1, 1),
(4, 1, 'c4168b', 'Bahasa Inggris', '12 IPA 6', '2016/2017', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `role` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `token` varchar(32) NOT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `email`, `password`, `role`, `status`, `token`) VALUES
(26, 'testerguru@gmail.com', '200820e3227815ed1756a6b531e7e0d2', 1, 1, '5cbfe9ff6d08fa5'),
(27, 'testersiswa1@gmail.com', '200820e3227815ed1756a6b531e7e0d2', 2, 1, '903db7f3a8eec00'),
(76, 'testersiswa2@gmail.com', '200820e3227815ed1756a6b531e7e0d2', 2, 1, 'a9ebbef728e44e8');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tmpt_lhr` varchar(30) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `jk` char(1) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `profilpict` varchar(35) NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nama_lengkap`, `email`, `tmpt_lhr`, `tgl_lhr`, `jk`, `telp`, `profilpict`) VALUES
(2, 'Rahmad Pamungkas', 'testersiswa1@gmail.com', 'Pasuruan', '1994-02-09', '1', '085707617310', ''),
(50, 'Andika Bagus', 'testersiswa2@gmail.com', 'Pandaan', '1994-09-02', '1', '085707617310', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
