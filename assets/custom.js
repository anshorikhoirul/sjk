//set active menu sidebar
$('ul.sidebar-menu li a').each(function(){
	var url = window.location.href;
	var menu = $(this).attr('href');
	if(url.match(menu)){
		if($(this).parent().parent().hasClass('treeview-menu')){
			$(this).parent().parent().parent().addClass('active');
		}
		$(this).parent().addClass('active');
	}
});