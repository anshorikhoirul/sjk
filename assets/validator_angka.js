function setInputNumeric($this){
    $this.keydown(function(event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
            // Allow only backspace and delete
        } else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }
        }
    });
}
$(document).ready(function(){
    setInputNumeric($('.numberonly'));
});